﻿Imports System.Data.SQLite

Public Class Artist
    Private _name As String

    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    Public Shared Function GetArtistNames(ByVal ConnectionString As String) As AutoCompleteStringCollection
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim ListArtists As String = "SELECT * FROM artists"
            Dim cmd As New SQLiteCommand(ListArtists, Sqlconn)
            Dim da As New SQLiteDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            Dim col As New AutoCompleteStringCollection
            da.Fill(dt)
            For i = 0 To dt.Rows.Count - 1
                col.Add(dt.Rows(i)("Name").ToString())
            Next
            Sqlconn.Close()
            Return col
        End Using
    End Function

    Public Shared Function GetArtistId(ByVal ConnectionString As String, ByVal name As String) As Integer
        Dim id As Integer
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getId As String = "SELECT ArtistId FROM artists WHERE Name = @Name"
            Dim Artistscmd As New SQLiteCommand(getId, Sqlconn)
            Artistscmd.Parameters.AddWithValue("@Name", name)
            Dim MediaTypeReader As SQLiteDataReader = Artistscmd.ExecuteReader()
            Using MediaTypeReader
                While (MediaTypeReader.Read())
                    id = MediaTypeReader.GetInt32(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return id
    End Function
End Class
