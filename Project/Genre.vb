﻿Imports System.Data.SQLite

Public Class Genre
    ''' <summary>
    ''' Name of the genre
    ''' </summary>
    Private _name As String

    Public Property Name As Integer
        Get
            Return _name
        End Get
        Set(value As Integer)
            _name = value
        End Set
    End Property

    Public Shared Function getGenres(ByVal ConnectionString As String) As DataTable
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim ListGenres As String = "SELECT * FROM genres"
            Dim cmd As New SQLiteCommand(ListGenres, Sqlconn)
            Dim da As New SQLiteDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            da.Fill(dt)
            Sqlconn.Close()
            Return dt
        End Using
    End Function

    Public Shared Function GetGenre(ByVal ConnectionString As String, ByVal id As Integer) As String
        Dim genre As String
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getGenreName As String = "SELECT Name FROM genres WHERE GenreId = @GenreId"
            Dim cmd As New SQLiteCommand(getGenreName, Sqlconn)
            cmd.Parameters.AddWithValue("@GenreId", id)
            Dim Reader As SQLiteDataReader = cmd.ExecuteReader()
            Using Reader
                While (Reader.Read())
                    genre = Reader.GetString(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return genre
    End Function

    Public Shared Function GetGenreId(ByVal ConnectionString As String, ByVal name As String) As Integer
        Dim id As Integer
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getID As String = "SELECT GenreId FROM genres WHERE Name = @Name"
            Dim Genrecmd As New SQLiteCommand(getID, Sqlconn)
            Genrecmd.Parameters.AddWithValue("@Name", name)
            Dim GenreReader As SQLiteDataReader = Genrecmd.ExecuteReader()
            Using GenreReader
                While (GenreReader.Read())
                    id = GenreReader.GetInt32(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return id
    End Function

End Class
