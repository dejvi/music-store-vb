﻿Public Class Employee
    Private _firstName As String
    Private _lastName As String
    Private _title As String
    Private _phone As String
    Private _email As String
    Private _dateOfBirth As Date
    Private _hireDate As Date
    Private _address As String
    Public Shared currentEmplyoeeId As Integer

    Public Property FirstName As String
        Get
            Return _firstName
        End Get
        Set(value As String)
            _firstName = value
        End Set
    End Property

    Public Property LastName As String
        Get
            Return _lastName
        End Get
        Set(value As String)
            _lastName = value
        End Set
    End Property

    Public Property Title As String
        Get
            Return _title
        End Get
        Set(value As String)
            _title = value
        End Set
    End Property

    Public Property Phone As String
        Get
            Return _phone
        End Get
        Set(value As String)
            _phone = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property DateOfBirth As Date
        Get
            Return _dateOfBirth
        End Get
        Set(value As Date)
            _dateOfBirth = value
        End Set
    End Property

    Public Property HireDate As Date
        Get
            Return _hireDate
        End Get
        Set(value As Date)
            _hireDate = value
        End Set
    End Property

    Public Property Address As String
        Get
            Return _address
        End Get
        Set(value As String)
            _address = value
        End Set
    End Property
End Class
