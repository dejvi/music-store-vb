﻿Imports System.Data.SQLite

Public Class Invoice
    Private _date As Date
    Private _total As Decimal



    Public Property Track As Track
        Get
            Return Nothing
        End Get
        Set(value As Track)
        End Set
    End Property

    Public Property Customer As Customer
        Get
            Return Nothing
        End Get
        Set(value As Customer)
        End Set
    End Property

    Public Property Employee As Employee
        Get
            Return Nothing
        End Get
        Set(value As Employee)
        End Set
    End Property

    Public Shared Sub addTrack(ByVal ConnectionString As String, ByVal CustomerId As Integer, ByVal Total As Decimal, ByVal d As DateTime)
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim insertInvoice As String = "INSERT INTO invoices(CustomerId, InvoiceDate, BillingAddress, BillingCity, BillingState, BillingCountry, BillingPostalCode, Total) 
                                           VALUES (@CustomerId, @InvoiceDate, 
                                           (SELECT Address FROM customers WHERE CustomerId = @CustomerId), 
                                           (SELECT City FROM customers WHERE CustomerId = @CustomerId), 
                                           (SELECT State FROM customers WHERE CustomerId = @CustomerId), 
                                           (SELECT Country FROM customers WHERE CustomerId = @CustomerId), 
                                           (SELECT PostalCode FROM customers WHERE CustomerId = @CustomerId), @Total)"
            Dim cmd As New SQLiteCommand(insertInvoice, Sqlconn)
            cmd.Parameters.AddWithValue("@CustomerId", CustomerId)
            cmd.Parameters.AddWithValue("@InvoiceDate", d)
            cmd.Parameters.AddWithValue("@Total", Total)
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

End Class
