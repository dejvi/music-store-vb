﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GUI))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPageCustomers = New System.Windows.Forms.TabPage()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPageAddCustomer = New System.Windows.Forms.TabPage()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ButtonClearCustomersT1 = New System.Windows.Forms.Button()
        Me.TextBoxPostalCode = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.TextBoxState = New System.Windows.Forms.TextBox()
        Me.TextBoxCity = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TextBoxFax = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.ConfirmButton = New System.Windows.Forms.Button()
        Me.TextBoxCompany = New System.Windows.Forms.TextBox()
        Me.TextBoxAddress = New System.Windows.Forms.TextBox()
        Me.TextBoxCountry = New System.Windows.Forms.TextBox()
        Me.TextBoxPhone = New System.Windows.Forms.TextBox()
        Me.TextBoxEmail = New System.Windows.Forms.TextBox()
        Me.TextBoxLastName = New System.Windows.Forms.TextBox()
        Me.TextBoxFirstName = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPageViewCustomer = New System.Windows.Forms.TabPage()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.ButtonClearT2 = New System.Windows.Forms.Button()
        Me.TextBoxPCT2 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxST2 = New System.Windows.Forms.TextBox()
        Me.TextBoxCityT2 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBoxFT2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBoxCompT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxAT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxCT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxPT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxET2 = New System.Windows.Forms.TextBox()
        Me.TextBoxLNT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxFNT2 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.ButtonView = New System.Windows.Forms.Button()
        Me.TextBoxCustomerID = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TabPageRemoveCustomer = New System.Windows.Forms.TabPage()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.TextBoxPCT3 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBoxST3 = New System.Windows.Forms.TextBox()
        Me.TextBoxCityT3 = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBoxFT3 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TextBoxCOT3 = New System.Windows.Forms.TextBox()
        Me.TextBoxAT3 = New System.Windows.Forms.TextBox()
        Me.TextBoxCT3 = New System.Windows.Forms.TextBox()
        Me.TextBoxPT3 = New System.Windows.Forms.TextBox()
        Me.TextBoxET3 = New System.Windows.Forms.TextBox()
        Me.TextBoxLNT3 = New System.Windows.Forms.TextBox()
        Me.TextBoxFNT3 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.ButtonDelete = New System.Windows.Forms.Button()
        Me.Search = New System.Windows.Forms.Button()
        Me.TextBoxIDRemove = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TabPageEditCustomer = New System.Windows.Forms.TabPage()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.TextBoxPCT4 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBoxST4 = New System.Windows.Forms.TextBox()
        Me.TextBoxCityT4 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBoxFT4 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBoxCOT4 = New System.Windows.Forms.TextBox()
        Me.TextBoxAT4 = New System.Windows.Forms.TextBox()
        Me.TextBoxCT4 = New System.Windows.Forms.TextBox()
        Me.TextBoxPT4 = New System.Windows.Forms.TextBox()
        Me.TextBoxET4 = New System.Windows.Forms.TextBox()
        Me.TextBoxLNT4 = New System.Windows.Forms.TextBox()
        Me.TextBoxFNT4 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.ButtonEdit = New System.Windows.Forms.Button()
        Me.ButtonViewt4 = New System.Windows.Forms.Button()
        Me.TextBoxIDt4 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TabPageListAllCustomers = New System.Windows.Forms.TabPage()
        Me.DataGridViewCustomers = New System.Windows.Forms.DataGridView()
        Me.TabPageInvoices = New System.Windows.Forms.TabPage()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.ButtonPrint = New System.Windows.Forms.Button()
        Me.ButtonCreateInvoice = New System.Windows.Forms.Button()
        Me.ButtonClearInvoices = New System.Windows.Forms.Button()
        Me.DataGridViewInvoices = New System.Windows.Forms.DataGridView()
        Me.ButtonAddToInvoices = New System.Windows.Forms.Button()
        Me.TextBoxQuantityInvoices = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.TextBoxTrackIdInvoices = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.TextBoxCustomerIdInvoices = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.TabPageSongs = New System.Windows.Forms.TabPage()
        Me.TabControl3 = New System.Windows.Forms.TabControl()
        Me.TabPageAddTrack = New System.Windows.Forms.TabPage()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ButtonAddAlbum = New System.Windows.Forms.Button()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.TextBoxArtistName = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.TextBoxAlbumName = New System.Windows.Forms.TextBox()
        Me.TextBoxAlbumNameT1 = New System.Windows.Forms.TextBox()
        Me.ButtonAddTrack = New System.Windows.Forms.Button()
        Me.TextBoxPriceT1 = New System.Windows.Forms.TextBox()
        Me.ComboBoxGenreT1 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxMediaTypeT1 = New System.Windows.Forms.ComboBox()
        Me.TextBoxComposerT1 = New System.Windows.Forms.TextBox()
        Me.TextBoxSizeT1 = New System.Windows.Forms.TextBox()
        Me.TextBoxDurationT1 = New System.Windows.Forms.TextBox()
        Me.TextBoxTrackNameT1 = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.LabelTrackNameT1 = New System.Windows.Forms.Label()
        Me.TabPageEditTrack = New System.Windows.Forms.TabPage()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.ButtonClearTracks = New System.Windows.Forms.Button()
        Me.ButtonDeleteTrack = New System.Windows.Forms.Button()
        Me.ButtonEditTrack = New System.Windows.Forms.Button()
        Me.ButtonSearchTrack = New System.Windows.Forms.Button()
        Me.TextBoxAlbumNameT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxPriceT2 = New System.Windows.Forms.TextBox()
        Me.ComboBoxGenreT2 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxMediaTypeT2 = New System.Windows.Forms.ComboBox()
        Me.TextBoxComposerT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxSizeT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxDurationT2 = New System.Windows.Forms.TextBox()
        Me.TextBoxTrackNameT2 = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.TextBoxTrackIdT2 = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TabPageListAllTracks = New System.Windows.Forms.TabPage()
        Me.DataGridViewListAllTracks = New System.Windows.Forms.DataGridView()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.TabControl1.SuspendLayout()
        Me.TabPageCustomers.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPageAddCustomer.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageViewCustomer.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageRemoveCustomer.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageEditCustomer.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageListAllCustomers.SuspendLayout()
        CType(Me.DataGridViewCustomers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageInvoices.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridViewInvoices, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageSongs.SuspendLayout()
        Me.TabControl3.SuspendLayout()
        Me.TabPageAddTrack.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabPageEditTrack.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageListAllTracks.SuspendLayout()
        CType(Me.DataGridViewListAllTracks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPageCustomers)
        Me.TabControl1.Controls.Add(Me.TabPageInvoices)
        Me.TabControl1.Controls.Add(Me.TabPageSongs)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(805, 538)
        Me.TabControl1.TabIndex = 0
        '
        'TabPageCustomers
        '
        Me.TabPageCustomers.Controls.Add(Me.TabControl2)
        Me.TabPageCustomers.Location = New System.Drawing.Point(4, 22)
        Me.TabPageCustomers.Name = "TabPageCustomers"
        Me.TabPageCustomers.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageCustomers.Size = New System.Drawing.Size(797, 512)
        Me.TabPageCustomers.TabIndex = 0
        Me.TabPageCustomers.Text = "Customers"
        Me.TabPageCustomers.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.TabControl2.Controls.Add(Me.TabPageAddCustomer)
        Me.TabControl2.Controls.Add(Me.TabPageViewCustomer)
        Me.TabControl2.Controls.Add(Me.TabPageRemoveCustomer)
        Me.TabControl2.Controls.Add(Me.TabPageEditCustomer)
        Me.TabControl2.Controls.Add(Me.TabPageListAllCustomers)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl2.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl2.ItemSize = New System.Drawing.Size(40, 165)
        Me.TabControl2.Location = New System.Drawing.Point(3, 3)
        Me.TabControl2.Multiline = True
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(791, 506)
        Me.TabControl2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.TabControl2.TabIndex = 0
        '
        'TabPageAddCustomer
        '
        Me.TabPageAddCustomer.Controls.Add(Me.PictureBox1)
        Me.TabPageAddCustomer.Controls.Add(Me.ButtonClearCustomersT1)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxPostalCode)
        Me.TabPageAddCustomer.Controls.Add(Me.Label35)
        Me.TabPageAddCustomer.Controls.Add(Me.Label34)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxState)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxCity)
        Me.TabPageAddCustomer.Controls.Add(Me.Label33)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxFax)
        Me.TabPageAddCustomer.Controls.Add(Me.Label32)
        Me.TabPageAddCustomer.Controls.Add(Me.ConfirmButton)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxCompany)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxAddress)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxCountry)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxPhone)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxEmail)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxLastName)
        Me.TabPageAddCustomer.Controls.Add(Me.TextBoxFirstName)
        Me.TabPageAddCustomer.Controls.Add(Me.Label7)
        Me.TabPageAddCustomer.Controls.Add(Me.Label6)
        Me.TabPageAddCustomer.Controls.Add(Me.Label5)
        Me.TabPageAddCustomer.Controls.Add(Me.Label4)
        Me.TabPageAddCustomer.Controls.Add(Me.Label3)
        Me.TabPageAddCustomer.Controls.Add(Me.Label2)
        Me.TabPageAddCustomer.Controls.Add(Me.Label1)
        Me.TabPageAddCustomer.Location = New System.Drawing.Point(169, 4)
        Me.TabPageAddCustomer.Name = "TabPageAddCustomer"
        Me.TabPageAddCustomer.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageAddCustomer.Size = New System.Drawing.Size(618, 498)
        Me.TabPageAddCustomer.TabIndex = 0
        Me.TabPageAddCustomer.Text = "Add a Customer"
        Me.TabPageAddCustomer.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'ButtonClearCustomersT1
        '
        Me.ButtonClearCustomersT1.Location = New System.Drawing.Point(207, 381)
        Me.ButtonClearCustomersT1.Name = "ButtonClearCustomersT1"
        Me.ButtonClearCustomersT1.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClearCustomersT1.TabIndex = 23
        Me.ButtonClearCustomersT1.Text = "Clear"
        Me.ButtonClearCustomersT1.UseVisualStyleBackColor = True
        '
        'TextBoxPostalCode
        '
        Me.TextBoxPostalCode.Location = New System.Drawing.Point(408, 273)
        Me.TextBoxPostalCode.Name = "TextBoxPostalCode"
        Me.TextBoxPostalCode.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxPostalCode.TabIndex = 22
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(319, 276)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(67, 13)
        Me.Label35.TabIndex = 21
        Me.Label35.Text = "Postal Code:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(351, 235)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(35, 13)
        Me.Label34.TabIndex = 20
        Me.Label34.Text = "State:"
        '
        'TextBoxState
        '
        Me.TextBoxState.Location = New System.Drawing.Point(408, 232)
        Me.TextBoxState.Name = "TextBoxState"
        Me.TextBoxState.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxState.TabIndex = 19
        '
        'TextBoxCity
        '
        Me.TextBoxCity.Location = New System.Drawing.Point(408, 189)
        Me.TextBoxCity.Name = "TextBoxCity"
        Me.TextBoxCity.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxCity.TabIndex = 18
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(359, 192)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(27, 13)
        Me.Label33.TabIndex = 17
        Me.Label33.Text = "City:"
        '
        'TextBoxFax
        '
        Me.TextBoxFax.Location = New System.Drawing.Point(408, 143)
        Me.TextBoxFax.Name = "TextBoxFax"
        Me.TextBoxFax.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxFax.TabIndex = 16
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(359, 146)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(27, 13)
        Me.Label32.TabIndex = 15
        Me.Label32.Text = "Fax:"
        '
        'ConfirmButton
        '
        Me.ConfirmButton.Location = New System.Drawing.Point(335, 381)
        Me.ConfirmButton.Name = "ConfirmButton"
        Me.ConfirmButton.Size = New System.Drawing.Size(75, 23)
        Me.ConfirmButton.TabIndex = 14
        Me.ConfirmButton.Text = "Confirm"
        Me.ConfirmButton.UseVisualStyleBackColor = True
        '
        'TextBoxCompany
        '
        Me.TextBoxCompany.Location = New System.Drawing.Point(408, 100)
        Me.TextBoxCompany.Name = "TextBoxCompany"
        Me.TextBoxCompany.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxCompany.TabIndex = 13
        '
        'TextBoxAddress
        '
        Me.TextBoxAddress.Location = New System.Drawing.Point(157, 318)
        Me.TextBoxAddress.Name = "TextBoxAddress"
        Me.TextBoxAddress.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxAddress.TabIndex = 12
        '
        'TextBoxCountry
        '
        Me.TextBoxCountry.Location = New System.Drawing.Point(157, 273)
        Me.TextBoxCountry.Name = "TextBoxCountry"
        Me.TextBoxCountry.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxCountry.TabIndex = 11
        '
        'TextBoxPhone
        '
        Me.TextBoxPhone.Location = New System.Drawing.Point(157, 232)
        Me.TextBoxPhone.Name = "TextBoxPhone"
        Me.TextBoxPhone.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxPhone.TabIndex = 10
        '
        'TextBoxEmail
        '
        Me.TextBoxEmail.Location = New System.Drawing.Point(157, 189)
        Me.TextBoxEmail.Name = "TextBoxEmail"
        Me.TextBoxEmail.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxEmail.TabIndex = 9
        '
        'TextBoxLastName
        '
        Me.TextBoxLastName.Location = New System.Drawing.Point(157, 143)
        Me.TextBoxLastName.Name = "TextBoxLastName"
        Me.TextBoxLastName.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxLastName.TabIndex = 8
        '
        'TextBoxFirstName
        '
        Me.TextBoxFirstName.Location = New System.Drawing.Point(157, 100)
        Me.TextBoxFirstName.Name = "TextBoxFirstName"
        Me.TextBoxFirstName.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxFirstName.TabIndex = 7
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(332, 103)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Company:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(80, 321)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Address: *"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(80, 276)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Country: *"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(87, 235)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Phone: *"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(93, 192)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Email: *"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(67, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Last Name: *"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(66, 103)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "First Name: *"
        '
        'TabPageViewCustomer
        '
        Me.TabPageViewCustomer.Controls.Add(Me.PictureBox2)
        Me.TabPageViewCustomer.Controls.Add(Me.ButtonClearT2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxPCT2)
        Me.TabPageViewCustomer.Controls.Add(Me.Label8)
        Me.TabPageViewCustomer.Controls.Add(Me.Label9)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxST2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxCityT2)
        Me.TabPageViewCustomer.Controls.Add(Me.Label10)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxFT2)
        Me.TabPageViewCustomer.Controls.Add(Me.Label11)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxCompT2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxAT2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxCT2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxPT2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxET2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxLNT2)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxFNT2)
        Me.TabPageViewCustomer.Controls.Add(Me.Label12)
        Me.TabPageViewCustomer.Controls.Add(Me.Label13)
        Me.TabPageViewCustomer.Controls.Add(Me.Label14)
        Me.TabPageViewCustomer.Controls.Add(Me.Label36)
        Me.TabPageViewCustomer.Controls.Add(Me.Label37)
        Me.TabPageViewCustomer.Controls.Add(Me.Label38)
        Me.TabPageViewCustomer.Controls.Add(Me.Label39)
        Me.TabPageViewCustomer.Controls.Add(Me.ButtonView)
        Me.TabPageViewCustomer.Controls.Add(Me.TextBoxCustomerID)
        Me.TabPageViewCustomer.Controls.Add(Me.Label15)
        Me.TabPageViewCustomer.Location = New System.Drawing.Point(169, 4)
        Me.TabPageViewCustomer.Name = "TabPageViewCustomer"
        Me.TabPageViewCustomer.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageViewCustomer.Size = New System.Drawing.Size(618, 498)
        Me.TabPageViewCustomer.TabIndex = 1
        Me.TabPageViewCustomer.Text = "View Customer Details"
        Me.TabPageViewCustomer.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox2.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox2.TabIndex = 47
        Me.PictureBox2.TabStop = False
        '
        'ButtonClearT2
        '
        Me.ButtonClearT2.Location = New System.Drawing.Point(390, 129)
        Me.ButtonClearT2.Name = "ButtonClearT2"
        Me.ButtonClearT2.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClearT2.TabIndex = 46
        Me.ButtonClearT2.Text = "Clear"
        Me.ButtonClearT2.UseVisualStyleBackColor = True
        '
        'TextBoxPCT2
        '
        Me.TextBoxPCT2.Location = New System.Drawing.Point(390, 321)
        Me.TextBoxPCT2.Name = "TextBoxPCT2"
        Me.TextBoxPCT2.ReadOnly = True
        Me.TextBoxPCT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxPCT2.TabIndex = 45
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(301, 324)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 13)
        Me.Label8.TabIndex = 44
        Me.Label8.Text = "Postal Code:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(333, 298)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 43
        Me.Label9.Text = "State:"
        '
        'TextBoxST2
        '
        Me.TextBoxST2.Location = New System.Drawing.Point(390, 295)
        Me.TextBoxST2.Name = "TextBoxST2"
        Me.TextBoxST2.ReadOnly = True
        Me.TextBoxST2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxST2.TabIndex = 42
        '
        'TextBoxCityT2
        '
        Me.TextBoxCityT2.Location = New System.Drawing.Point(390, 269)
        Me.TextBoxCityT2.Name = "TextBoxCityT2"
        Me.TextBoxCityT2.ReadOnly = True
        Me.TextBoxCityT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxCityT2.TabIndex = 41
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(341, 272)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(27, 13)
        Me.Label10.TabIndex = 40
        Me.Label10.Text = "City:"
        '
        'TextBoxFT2
        '
        Me.TextBoxFT2.Location = New System.Drawing.Point(390, 243)
        Me.TextBoxFT2.Name = "TextBoxFT2"
        Me.TextBoxFT2.ReadOnly = True
        Me.TextBoxFT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxFT2.TabIndex = 39
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(341, 246)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(27, 13)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Fax:"
        '
        'TextBoxCompT2
        '
        Me.TextBoxCompT2.Location = New System.Drawing.Point(390, 217)
        Me.TextBoxCompT2.Name = "TextBoxCompT2"
        Me.TextBoxCompT2.ReadOnly = True
        Me.TextBoxCompT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxCompT2.TabIndex = 37
        '
        'TextBoxAT2
        '
        Me.TextBoxAT2.Location = New System.Drawing.Point(139, 345)
        Me.TextBoxAT2.Name = "TextBoxAT2"
        Me.TextBoxAT2.ReadOnly = True
        Me.TextBoxAT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxAT2.TabIndex = 36
        '
        'TextBoxCT2
        '
        Me.TextBoxCT2.Location = New System.Drawing.Point(139, 316)
        Me.TextBoxCT2.Name = "TextBoxCT2"
        Me.TextBoxCT2.ReadOnly = True
        Me.TextBoxCT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxCT2.TabIndex = 35
        '
        'TextBoxPT2
        '
        Me.TextBoxPT2.Location = New System.Drawing.Point(139, 290)
        Me.TextBoxPT2.Name = "TextBoxPT2"
        Me.TextBoxPT2.ReadOnly = True
        Me.TextBoxPT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxPT2.TabIndex = 34
        '
        'TextBoxET2
        '
        Me.TextBoxET2.Location = New System.Drawing.Point(139, 267)
        Me.TextBoxET2.Name = "TextBoxET2"
        Me.TextBoxET2.ReadOnly = True
        Me.TextBoxET2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxET2.TabIndex = 33
        '
        'TextBoxLNT2
        '
        Me.TextBoxLNT2.Location = New System.Drawing.Point(139, 241)
        Me.TextBoxLNT2.Name = "TextBoxLNT2"
        Me.TextBoxLNT2.ReadOnly = True
        Me.TextBoxLNT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxLNT2.TabIndex = 32
        '
        'TextBoxFNT2
        '
        Me.TextBoxFNT2.Location = New System.Drawing.Point(139, 217)
        Me.TextBoxFNT2.Name = "TextBoxFNT2"
        Me.TextBoxFNT2.ReadOnly = True
        Me.TextBoxFNT2.Size = New System.Drawing.Size(128, 20)
        Me.TextBoxFNT2.TabIndex = 31
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(314, 220)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(54, 13)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "Company:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(62, 345)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(48, 13)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "Address:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(64, 319)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(46, 13)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "Country:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(69, 293)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(41, 13)
        Me.Label36.TabIndex = 27
        Me.Label36.Text = "Phone:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(75, 270)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(35, 13)
        Me.Label37.TabIndex = 26
        Me.Label37.Text = "Email:"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(49, 245)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(61, 13)
        Me.Label38.TabIndex = 25
        Me.Label38.Text = "Last Name:"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(50, 220)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(60, 13)
        Me.Label39.TabIndex = 24
        Me.Label39.Text = "First Name:"
        '
        'ButtonView
        '
        Me.ButtonView.Location = New System.Drawing.Point(275, 129)
        Me.ButtonView.Name = "ButtonView"
        Me.ButtonView.Size = New System.Drawing.Size(75, 23)
        Me.ButtonView.TabIndex = 23
        Me.ButtonView.Text = "View"
        Me.ButtonView.UseVisualStyleBackColor = True
        '
        'TextBoxCustomerID
        '
        Me.TextBoxCustomerID.Location = New System.Drawing.Point(139, 131)
        Me.TextBoxCustomerID.Name = "TextBoxCustomerID"
        Me.TextBoxCustomerID.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxCustomerID.TabIndex = 15
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(42, 131)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 13)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Customer ID:"
        '
        'TabPageRemoveCustomer
        '
        Me.TabPageRemoveCustomer.Controls.Add(Me.PictureBox3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxPCT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label24)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label25)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxST3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxCityT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label26)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxFT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label27)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxCOT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxAT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxCT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxPT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxET3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxLNT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxFNT3)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label28)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label29)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label30)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label40)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label41)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label42)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label43)
        Me.TabPageRemoveCustomer.Controls.Add(Me.ButtonDelete)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Search)
        Me.TabPageRemoveCustomer.Controls.Add(Me.TextBoxIDRemove)
        Me.TabPageRemoveCustomer.Controls.Add(Me.Label23)
        Me.TabPageRemoveCustomer.Location = New System.Drawing.Point(169, 4)
        Me.TabPageRemoveCustomer.Name = "TabPageRemoveCustomer"
        Me.TabPageRemoveCustomer.Size = New System.Drawing.Size(618, 498)
        Me.TabPageRemoveCustomer.TabIndex = 2
        Me.TabPageRemoveCustomer.Text = "Remove a Customer"
        Me.TabPageRemoveCustomer.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox3.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox3.TabIndex = 68
        Me.PictureBox3.TabStop = False
        '
        'TextBoxPCT3
        '
        Me.TextBoxPCT3.Location = New System.Drawing.Point(400, 296)
        Me.TextBoxPCT3.Name = "TextBoxPCT3"
        Me.TextBoxPCT3.ReadOnly = True
        Me.TextBoxPCT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxPCT3.TabIndex = 67
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(311, 299)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(67, 13)
        Me.Label24.TabIndex = 66
        Me.Label24.Text = "Postal Code:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(343, 273)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(35, 13)
        Me.Label25.TabIndex = 65
        Me.Label25.Text = "State:"
        '
        'TextBoxST3
        '
        Me.TextBoxST3.Location = New System.Drawing.Point(400, 270)
        Me.TextBoxST3.Name = "TextBoxST3"
        Me.TextBoxST3.ReadOnly = True
        Me.TextBoxST3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxST3.TabIndex = 64
        '
        'TextBoxCityT3
        '
        Me.TextBoxCityT3.Location = New System.Drawing.Point(400, 244)
        Me.TextBoxCityT3.Name = "TextBoxCityT3"
        Me.TextBoxCityT3.ReadOnly = True
        Me.TextBoxCityT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxCityT3.TabIndex = 63
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(351, 247)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(27, 13)
        Me.Label26.TabIndex = 62
        Me.Label26.Text = "City:"
        '
        'TextBoxFT3
        '
        Me.TextBoxFT3.Location = New System.Drawing.Point(400, 218)
        Me.TextBoxFT3.Name = "TextBoxFT3"
        Me.TextBoxFT3.ReadOnly = True
        Me.TextBoxFT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxFT3.TabIndex = 61
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(351, 221)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(27, 13)
        Me.Label27.TabIndex = 60
        Me.Label27.Text = "Fax:"
        '
        'TextBoxCOT3
        '
        Me.TextBoxCOT3.Location = New System.Drawing.Point(400, 192)
        Me.TextBoxCOT3.Name = "TextBoxCOT3"
        Me.TextBoxCOT3.ReadOnly = True
        Me.TextBoxCOT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxCOT3.TabIndex = 59
        '
        'TextBoxAT3
        '
        Me.TextBoxAT3.Location = New System.Drawing.Point(149, 320)
        Me.TextBoxAT3.Name = "TextBoxAT3"
        Me.TextBoxAT3.ReadOnly = True
        Me.TextBoxAT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxAT3.TabIndex = 58
        '
        'TextBoxCT3
        '
        Me.TextBoxCT3.Location = New System.Drawing.Point(149, 291)
        Me.TextBoxCT3.Name = "TextBoxCT3"
        Me.TextBoxCT3.ReadOnly = True
        Me.TextBoxCT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxCT3.TabIndex = 57
        '
        'TextBoxPT3
        '
        Me.TextBoxPT3.Location = New System.Drawing.Point(149, 265)
        Me.TextBoxPT3.Name = "TextBoxPT3"
        Me.TextBoxPT3.ReadOnly = True
        Me.TextBoxPT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxPT3.TabIndex = 56
        '
        'TextBoxET3
        '
        Me.TextBoxET3.Location = New System.Drawing.Point(149, 242)
        Me.TextBoxET3.Name = "TextBoxET3"
        Me.TextBoxET3.ReadOnly = True
        Me.TextBoxET3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxET3.TabIndex = 55
        '
        'TextBoxLNT3
        '
        Me.TextBoxLNT3.Location = New System.Drawing.Point(149, 216)
        Me.TextBoxLNT3.Name = "TextBoxLNT3"
        Me.TextBoxLNT3.ReadOnly = True
        Me.TextBoxLNT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxLNT3.TabIndex = 54
        '
        'TextBoxFNT3
        '
        Me.TextBoxFNT3.Location = New System.Drawing.Point(149, 192)
        Me.TextBoxFNT3.Name = "TextBoxFNT3"
        Me.TextBoxFNT3.ReadOnly = True
        Me.TextBoxFNT3.Size = New System.Drawing.Size(126, 20)
        Me.TextBoxFNT3.TabIndex = 53
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(324, 195)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(54, 13)
        Me.Label28.TabIndex = 52
        Me.Label28.Text = "Company:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(72, 320)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 13)
        Me.Label29.TabIndex = 51
        Me.Label29.Text = "Address:"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(74, 294)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(46, 13)
        Me.Label30.TabIndex = 50
        Me.Label30.Text = "Country:"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(79, 268)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(41, 13)
        Me.Label40.TabIndex = 49
        Me.Label40.Text = "Phone:"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(85, 245)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(35, 13)
        Me.Label41.TabIndex = 48
        Me.Label41.Text = "Email:"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(59, 220)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(61, 13)
        Me.Label42.TabIndex = 47
        Me.Label42.Text = "Last Name:"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(60, 195)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(60, 13)
        Me.Label43.TabIndex = 46
        Me.Label43.Text = "First Name:"
        '
        'ButtonDelete
        '
        Me.ButtonDelete.Location = New System.Drawing.Point(238, 399)
        Me.ButtonDelete.Name = "ButtonDelete"
        Me.ButtonDelete.Size = New System.Drawing.Size(75, 23)
        Me.ButtonDelete.TabIndex = 41
        Me.ButtonDelete.Text = "Delete"
        Me.ButtonDelete.UseVisualStyleBackColor = True
        '
        'Search
        '
        Me.Search.Location = New System.Drawing.Point(309, 124)
        Me.Search.Name = "Search"
        Me.Search.Size = New System.Drawing.Size(75, 23)
        Me.Search.TabIndex = 40
        Me.Search.Text = "Search"
        Me.Search.UseVisualStyleBackColor = True
        '
        'TextBoxIDRemove
        '
        Me.TextBoxIDRemove.Location = New System.Drawing.Point(185, 124)
        Me.TextBoxIDRemove.Name = "TextBoxIDRemove"
        Me.TextBoxIDRemove.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxIDRemove.TabIndex = 32
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(52, 124)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(68, 13)
        Me.Label23.TabIndex = 31
        Me.Label23.Text = "Customer ID:"
        '
        'TabPageEditCustomer
        '
        Me.TabPageEditCustomer.Controls.Add(Me.PictureBox4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxPCT4)
        Me.TabPageEditCustomer.Controls.Add(Me.Label16)
        Me.TabPageEditCustomer.Controls.Add(Me.Label17)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxST4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxCityT4)
        Me.TabPageEditCustomer.Controls.Add(Me.Label18)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxFT4)
        Me.TabPageEditCustomer.Controls.Add(Me.Label19)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxCOT4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxAT4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxCT4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxPT4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxET4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxLNT4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxFNT4)
        Me.TabPageEditCustomer.Controls.Add(Me.Label20)
        Me.TabPageEditCustomer.Controls.Add(Me.Label21)
        Me.TabPageEditCustomer.Controls.Add(Me.Label22)
        Me.TabPageEditCustomer.Controls.Add(Me.Label44)
        Me.TabPageEditCustomer.Controls.Add(Me.Label45)
        Me.TabPageEditCustomer.Controls.Add(Me.Label46)
        Me.TabPageEditCustomer.Controls.Add(Me.Label47)
        Me.TabPageEditCustomer.Controls.Add(Me.ButtonEdit)
        Me.TabPageEditCustomer.Controls.Add(Me.ButtonViewt4)
        Me.TabPageEditCustomer.Controls.Add(Me.TextBoxIDt4)
        Me.TabPageEditCustomer.Controls.Add(Me.Label31)
        Me.TabPageEditCustomer.Location = New System.Drawing.Point(169, 4)
        Me.TabPageEditCustomer.Name = "TabPageEditCustomer"
        Me.TabPageEditCustomer.Size = New System.Drawing.Size(618, 498)
        Me.TabPageEditCustomer.TabIndex = 3
        Me.TabPageEditCustomer.Text = "Edit Customer Details"
        Me.TabPageEditCustomer.UseVisualStyleBackColor = True
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox4.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox4.TabIndex = 90
        Me.PictureBox4.TabStop = False
        '
        'TextBoxPCT4
        '
        Me.TextBoxPCT4.Location = New System.Drawing.Point(412, 314)
        Me.TextBoxPCT4.Name = "TextBoxPCT4"
        Me.TextBoxPCT4.ReadOnly = True
        Me.TextBoxPCT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxPCT4.TabIndex = 89
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(323, 317)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(67, 13)
        Me.Label16.TabIndex = 88
        Me.Label16.Text = "Postal Code:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(355, 291)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(35, 13)
        Me.Label17.TabIndex = 87
        Me.Label17.Text = "State:"
        '
        'TextBoxST4
        '
        Me.TextBoxST4.Location = New System.Drawing.Point(412, 288)
        Me.TextBoxST4.Name = "TextBoxST4"
        Me.TextBoxST4.ReadOnly = True
        Me.TextBoxST4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxST4.TabIndex = 86
        '
        'TextBoxCityT4
        '
        Me.TextBoxCityT4.Location = New System.Drawing.Point(412, 262)
        Me.TextBoxCityT4.Name = "TextBoxCityT4"
        Me.TextBoxCityT4.ReadOnly = True
        Me.TextBoxCityT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxCityT4.TabIndex = 85
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(363, 265)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(27, 13)
        Me.Label18.TabIndex = 84
        Me.Label18.Text = "City:"
        '
        'TextBoxFT4
        '
        Me.TextBoxFT4.Location = New System.Drawing.Point(412, 236)
        Me.TextBoxFT4.Name = "TextBoxFT4"
        Me.TextBoxFT4.ReadOnly = True
        Me.TextBoxFT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxFT4.TabIndex = 83
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(363, 239)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(27, 13)
        Me.Label19.TabIndex = 82
        Me.Label19.Text = "Fax:"
        '
        'TextBoxCOT4
        '
        Me.TextBoxCOT4.Location = New System.Drawing.Point(412, 210)
        Me.TextBoxCOT4.Name = "TextBoxCOT4"
        Me.TextBoxCOT4.ReadOnly = True
        Me.TextBoxCOT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxCOT4.TabIndex = 81
        '
        'TextBoxAT4
        '
        Me.TextBoxAT4.Location = New System.Drawing.Point(161, 338)
        Me.TextBoxAT4.Name = "TextBoxAT4"
        Me.TextBoxAT4.ReadOnly = True
        Me.TextBoxAT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxAT4.TabIndex = 80
        '
        'TextBoxCT4
        '
        Me.TextBoxCT4.Location = New System.Drawing.Point(161, 309)
        Me.TextBoxCT4.Name = "TextBoxCT4"
        Me.TextBoxCT4.ReadOnly = True
        Me.TextBoxCT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxCT4.TabIndex = 79
        '
        'TextBoxPT4
        '
        Me.TextBoxPT4.Location = New System.Drawing.Point(161, 283)
        Me.TextBoxPT4.Name = "TextBoxPT4"
        Me.TextBoxPT4.ReadOnly = True
        Me.TextBoxPT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxPT4.TabIndex = 78
        '
        'TextBoxET4
        '
        Me.TextBoxET4.Location = New System.Drawing.Point(161, 260)
        Me.TextBoxET4.Name = "TextBoxET4"
        Me.TextBoxET4.ReadOnly = True
        Me.TextBoxET4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxET4.TabIndex = 77
        '
        'TextBoxLNT4
        '
        Me.TextBoxLNT4.Location = New System.Drawing.Point(161, 234)
        Me.TextBoxLNT4.Name = "TextBoxLNT4"
        Me.TextBoxLNT4.ReadOnly = True
        Me.TextBoxLNT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxLNT4.TabIndex = 76
        '
        'TextBoxFNT4
        '
        Me.TextBoxFNT4.Location = New System.Drawing.Point(161, 210)
        Me.TextBoxFNT4.Name = "TextBoxFNT4"
        Me.TextBoxFNT4.ReadOnly = True
        Me.TextBoxFNT4.Size = New System.Drawing.Size(123, 20)
        Me.TextBoxFNT4.TabIndex = 75
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(336, 213)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(54, 13)
        Me.Label20.TabIndex = 74
        Me.Label20.Text = "Company:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(84, 338)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(55, 13)
        Me.Label21.TabIndex = 73
        Me.Label21.Text = "Address: *"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(86, 312)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(53, 13)
        Me.Label22.TabIndex = 72
        Me.Label22.Text = "Country: *"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(91, 286)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(48, 13)
        Me.Label44.TabIndex = 71
        Me.Label44.Text = "Phone: *"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(97, 263)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(42, 13)
        Me.Label45.TabIndex = 70
        Me.Label45.Text = "Email: *"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(71, 238)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(68, 13)
        Me.Label46.TabIndex = 69
        Me.Label46.Text = "Last Name: *"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(72, 213)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(67, 13)
        Me.Label47.TabIndex = 68
        Me.Label47.Text = "First Name: *"
        '
        'ButtonEdit
        '
        Me.ButtonEdit.Location = New System.Drawing.Point(259, 394)
        Me.ButtonEdit.Name = "ButtonEdit"
        Me.ButtonEdit.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEdit.TabIndex = 31
        Me.ButtonEdit.Text = "Edit"
        Me.ButtonEdit.UseVisualStyleBackColor = True
        '
        'ButtonViewt4
        '
        Me.ButtonViewt4.Location = New System.Drawing.Point(358, 125)
        Me.ButtonViewt4.Name = "ButtonViewt4"
        Me.ButtonViewt4.Size = New System.Drawing.Size(75, 23)
        Me.ButtonViewt4.TabIndex = 30
        Me.ButtonViewt4.Text = "View"
        Me.ButtonViewt4.UseVisualStyleBackColor = True
        '
        'TextBoxIDt4
        '
        Me.TextBoxIDt4.Location = New System.Drawing.Point(197, 125)
        Me.TextBoxIDt4.Name = "TextBoxIDt4"
        Me.TextBoxIDt4.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxIDt4.TabIndex = 29
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(64, 125)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(68, 13)
        Me.Label31.TabIndex = 28
        Me.Label31.Text = "Customer ID:"
        '
        'TabPageListAllCustomers
        '
        Me.TabPageListAllCustomers.Controls.Add(Me.DataGridViewCustomers)
        Me.TabPageListAllCustomers.Location = New System.Drawing.Point(169, 4)
        Me.TabPageListAllCustomers.Name = "TabPageListAllCustomers"
        Me.TabPageListAllCustomers.Size = New System.Drawing.Size(618, 498)
        Me.TabPageListAllCustomers.TabIndex = 4
        Me.TabPageListAllCustomers.Text = "List All Customers"
        Me.TabPageListAllCustomers.UseVisualStyleBackColor = True
        '
        'DataGridViewCustomers
        '
        Me.DataGridViewCustomers.AllowUserToAddRows = False
        Me.DataGridViewCustomers.AllowUserToDeleteRows = False
        Me.DataGridViewCustomers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewCustomers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridViewCustomers.Location = New System.Drawing.Point(0, 0)
        Me.DataGridViewCustomers.Name = "DataGridViewCustomers"
        Me.DataGridViewCustomers.ReadOnly = True
        Me.DataGridViewCustomers.Size = New System.Drawing.Size(618, 498)
        Me.DataGridViewCustomers.TabIndex = 0
        '
        'TabPageInvoices
        '
        Me.TabPageInvoices.Controls.Add(Me.PictureBox5)
        Me.TabPageInvoices.Controls.Add(Me.ButtonPrint)
        Me.TabPageInvoices.Controls.Add(Me.ButtonCreateInvoice)
        Me.TabPageInvoices.Controls.Add(Me.ButtonClearInvoices)
        Me.TabPageInvoices.Controls.Add(Me.DataGridViewInvoices)
        Me.TabPageInvoices.Controls.Add(Me.ButtonAddToInvoices)
        Me.TabPageInvoices.Controls.Add(Me.TextBoxQuantityInvoices)
        Me.TabPageInvoices.Controls.Add(Me.Label68)
        Me.TabPageInvoices.Controls.Add(Me.TextBoxTrackIdInvoices)
        Me.TabPageInvoices.Controls.Add(Me.Label67)
        Me.TabPageInvoices.Controls.Add(Me.TextBoxCustomerIdInvoices)
        Me.TabPageInvoices.Controls.Add(Me.Label66)
        Me.TabPageInvoices.Location = New System.Drawing.Point(4, 22)
        Me.TabPageInvoices.Name = "TabPageInvoices"
        Me.TabPageInvoices.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageInvoices.Size = New System.Drawing.Size(797, 512)
        Me.TabPageInvoices.TabIndex = 1
        Me.TabPageInvoices.Text = "Invoices"
        Me.TabPageInvoices.UseVisualStyleBackColor = True
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox5.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox5.TabIndex = 25
        Me.PictureBox5.TabStop = False
        '
        'ButtonPrint
        '
        Me.ButtonPrint.Location = New System.Drawing.Point(221, 447)
        Me.ButtonPrint.Name = "ButtonPrint"
        Me.ButtonPrint.Size = New System.Drawing.Size(75, 23)
        Me.ButtonPrint.TabIndex = 10
        Me.ButtonPrint.Text = "Print"
        Me.ButtonPrint.UseVisualStyleBackColor = True
        '
        'ButtonCreateInvoice
        '
        Me.ButtonCreateInvoice.Location = New System.Drawing.Point(479, 447)
        Me.ButtonCreateInvoice.Name = "ButtonCreateInvoice"
        Me.ButtonCreateInvoice.Size = New System.Drawing.Size(94, 23)
        Me.ButtonCreateInvoice.TabIndex = 9
        Me.ButtonCreateInvoice.Text = "Create Invoice"
        Me.ButtonCreateInvoice.UseVisualStyleBackColor = True
        '
        'ButtonClearInvoices
        '
        Me.ButtonClearInvoices.Location = New System.Drawing.Point(351, 447)
        Me.ButtonClearInvoices.Name = "ButtonClearInvoices"
        Me.ButtonClearInvoices.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClearInvoices.TabIndex = 8
        Me.ButtonClearInvoices.Text = "Clear"
        Me.ButtonClearInvoices.UseVisualStyleBackColor = True
        '
        'DataGridViewInvoices
        '
        Me.DataGridViewInvoices.AllowUserToAddRows = False
        Me.DataGridViewInvoices.AllowUserToDeleteRows = False
        Me.DataGridViewInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewInvoices.Location = New System.Drawing.Point(6, 129)
        Me.DataGridViewInvoices.Name = "DataGridViewInvoices"
        Me.DataGridViewInvoices.ReadOnly = True
        Me.DataGridViewInvoices.Size = New System.Drawing.Size(791, 293)
        Me.DataGridViewInvoices.TabIndex = 7
        '
        'ButtonAddToInvoices
        '
        Me.ButtonAddToInvoices.Location = New System.Drawing.Point(599, 77)
        Me.ButtonAddToInvoices.Name = "ButtonAddToInvoices"
        Me.ButtonAddToInvoices.Size = New System.Drawing.Size(81, 23)
        Me.ButtonAddToInvoices.TabIndex = 6
        Me.ButtonAddToInvoices.Text = "Add to List"
        Me.ButtonAddToInvoices.UseVisualStyleBackColor = True
        '
        'TextBoxQuantityInvoices
        '
        Me.TextBoxQuantityInvoices.Location = New System.Drawing.Point(485, 77)
        Me.TextBoxQuantityInvoices.Name = "TextBoxQuantityInvoices"
        Me.TextBoxQuantityInvoices.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxQuantityInvoices.TabIndex = 5
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(430, 80)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(49, 13)
        Me.Label68.TabIndex = 4
        Me.Label68.Text = "Quantity:"
        '
        'TextBoxTrackIdInvoices
        '
        Me.TextBoxTrackIdInvoices.Location = New System.Drawing.Point(319, 77)
        Me.TextBoxTrackIdInvoices.Name = "TextBoxTrackIdInvoices"
        Me.TextBoxTrackIdInvoices.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxTrackIdInvoices.TabIndex = 3
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(261, 80)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(52, 13)
        Me.Label67.TabIndex = 2
        Me.Label67.Text = "Track ID:"
        '
        'TextBoxCustomerIdInvoices
        '
        Me.TextBoxCustomerIdInvoices.Location = New System.Drawing.Point(151, 77)
        Me.TextBoxCustomerIdInvoices.Name = "TextBoxCustomerIdInvoices"
        Me.TextBoxCustomerIdInvoices.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxCustomerIdInvoices.TabIndex = 1
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(77, 80)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(68, 13)
        Me.Label66.TabIndex = 0
        Me.Label66.Text = "Customer ID:"
        '
        'TabPageSongs
        '
        Me.TabPageSongs.Controls.Add(Me.TabControl3)
        Me.TabPageSongs.Location = New System.Drawing.Point(4, 22)
        Me.TabPageSongs.Name = "TabPageSongs"
        Me.TabPageSongs.Size = New System.Drawing.Size(797, 512)
        Me.TabPageSongs.TabIndex = 2
        Me.TabPageSongs.Text = "Songs"
        Me.TabPageSongs.UseVisualStyleBackColor = True
        '
        'TabControl3
        '
        Me.TabControl3.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.TabControl3.Controls.Add(Me.TabPageAddTrack)
        Me.TabControl3.Controls.Add(Me.TabPageEditTrack)
        Me.TabControl3.Controls.Add(Me.TabPageListAllTracks)
        Me.TabControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl3.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl3.ItemSize = New System.Drawing.Size(40, 165)
        Me.TabControl3.Location = New System.Drawing.Point(0, 0)
        Me.TabControl3.Multiline = True
        Me.TabControl3.Name = "TabControl3"
        Me.TabControl3.SelectedIndex = 0
        Me.TabControl3.Size = New System.Drawing.Size(797, 512)
        Me.TabControl3.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.TabControl3.TabIndex = 0
        '
        'TabPageAddTrack
        '
        Me.TabPageAddTrack.Controls.Add(Me.PictureBox6)
        Me.TabPageAddTrack.Controls.Add(Me.GroupBox1)
        Me.TabPageAddTrack.Controls.Add(Me.TextBoxAlbumNameT1)
        Me.TabPageAddTrack.Controls.Add(Me.ButtonAddTrack)
        Me.TabPageAddTrack.Controls.Add(Me.TextBoxPriceT1)
        Me.TabPageAddTrack.Controls.Add(Me.ComboBoxGenreT1)
        Me.TabPageAddTrack.Controls.Add(Me.ComboBoxMediaTypeT1)
        Me.TabPageAddTrack.Controls.Add(Me.TextBoxComposerT1)
        Me.TabPageAddTrack.Controls.Add(Me.TextBoxSizeT1)
        Me.TabPageAddTrack.Controls.Add(Me.TextBoxDurationT1)
        Me.TabPageAddTrack.Controls.Add(Me.TextBoxTrackNameT1)
        Me.TabPageAddTrack.Controls.Add(Me.Label54)
        Me.TabPageAddTrack.Controls.Add(Me.Label53)
        Me.TabPageAddTrack.Controls.Add(Me.Label52)
        Me.TabPageAddTrack.Controls.Add(Me.Label51)
        Me.TabPageAddTrack.Controls.Add(Me.Label50)
        Me.TabPageAddTrack.Controls.Add(Me.Label49)
        Me.TabPageAddTrack.Controls.Add(Me.Label48)
        Me.TabPageAddTrack.Controls.Add(Me.LabelTrackNameT1)
        Me.TabPageAddTrack.Location = New System.Drawing.Point(169, 4)
        Me.TabPageAddTrack.Name = "TabPageAddTrack"
        Me.TabPageAddTrack.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageAddTrack.Size = New System.Drawing.Size(624, 504)
        Me.TabPageAddTrack.TabIndex = 0
        Me.TabPageAddTrack.Text = "Add Track"
        Me.TabPageAddTrack.UseVisualStyleBackColor = True
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox6.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox6.TabIndex = 25
        Me.PictureBox6.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ButtonAddAlbum)
        Me.GroupBox1.Controls.Add(Me.Label65)
        Me.GroupBox1.Controls.Add(Me.TextBoxArtistName)
        Me.GroupBox1.Controls.Add(Me.Label64)
        Me.GroupBox1.Controls.Add(Me.TextBoxAlbumName)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 306)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(582, 162)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Add Album"
        '
        'ButtonAddAlbum
        '
        Me.ButtonAddAlbum.Location = New System.Drawing.Point(243, 114)
        Me.ButtonAddAlbum.Name = "ButtonAddAlbum"
        Me.ButtonAddAlbum.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAddAlbum.TabIndex = 4
        Me.ButtonAddAlbum.Text = "Add Album"
        Me.ButtonAddAlbum.UseVisualStyleBackColor = True
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(338, 55)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(64, 13)
        Me.Label65.TabIndex = 3
        Me.Label65.Text = "Artist Name:"
        '
        'TextBoxArtistName
        '
        Me.TextBoxArtistName.Location = New System.Drawing.Point(408, 52)
        Me.TextBoxArtistName.Name = "TextBoxArtistName"
        Me.TextBoxArtistName.Size = New System.Drawing.Size(120, 20)
        Me.TextBoxArtistName.TabIndex = 2
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(53, 55)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(70, 13)
        Me.Label64.TabIndex = 1
        Me.Label64.Text = "Album Name:"
        '
        'TextBoxAlbumName
        '
        Me.TextBoxAlbumName.Location = New System.Drawing.Point(129, 52)
        Me.TextBoxAlbumName.Name = "TextBoxAlbumName"
        Me.TextBoxAlbumName.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxAlbumName.TabIndex = 0
        '
        'TextBoxAlbumNameT1
        '
        Me.TextBoxAlbumNameT1.Location = New System.Drawing.Point(135, 113)
        Me.TextBoxAlbumNameT1.Name = "TextBoxAlbumNameT1"
        Me.TextBoxAlbumNameT1.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxAlbumNameT1.TabIndex = 16
        '
        'ButtonAddTrack
        '
        Me.ButtonAddTrack.Location = New System.Drawing.Point(249, 255)
        Me.ButtonAddTrack.Name = "ButtonAddTrack"
        Me.ButtonAddTrack.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAddTrack.TabIndex = 15
        Me.ButtonAddTrack.Text = "Add Track"
        Me.ButtonAddTrack.UseVisualStyleBackColor = True
        '
        'TextBoxPriceT1
        '
        Me.TextBoxPriceT1.Location = New System.Drawing.Point(401, 151)
        Me.TextBoxPriceT1.Name = "TextBoxPriceT1"
        Me.TextBoxPriceT1.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxPriceT1.TabIndex = 14
        '
        'ComboBoxGenreT1
        '
        Me.ComboBoxGenreT1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxGenreT1.FormattingEnabled = True
        Me.ComboBoxGenreT1.Location = New System.Drawing.Point(135, 187)
        Me.ComboBoxGenreT1.Name = "ComboBoxGenreT1"
        Me.ComboBoxGenreT1.Size = New System.Drawing.Size(133, 21)
        Me.ComboBoxGenreT1.TabIndex = 13
        '
        'ComboBoxMediaTypeT1
        '
        Me.ComboBoxMediaTypeT1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMediaTypeT1.FormattingEnabled = True
        Me.ComboBoxMediaTypeT1.Location = New System.Drawing.Point(135, 151)
        Me.ComboBoxMediaTypeT1.Name = "ComboBoxMediaTypeT1"
        Me.ComboBoxMediaTypeT1.Size = New System.Drawing.Size(133, 21)
        Me.ComboBoxMediaTypeT1.TabIndex = 12
        '
        'TextBoxComposerT1
        '
        Me.TextBoxComposerT1.Location = New System.Drawing.Point(401, 187)
        Me.TextBoxComposerT1.Name = "TextBoxComposerT1"
        Me.TextBoxComposerT1.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxComposerT1.TabIndex = 11
        '
        'TextBoxSizeT1
        '
        Me.TextBoxSizeT1.Location = New System.Drawing.Point(401, 116)
        Me.TextBoxSizeT1.Name = "TextBoxSizeT1"
        Me.TextBoxSizeT1.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxSizeT1.TabIndex = 10
        '
        'TextBoxDurationT1
        '
        Me.TextBoxDurationT1.Location = New System.Drawing.Point(401, 74)
        Me.TextBoxDurationT1.Name = "TextBoxDurationT1"
        Me.TextBoxDurationT1.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxDurationT1.TabIndex = 9
        '
        'TextBoxTrackNameT1
        '
        Me.TextBoxTrackNameT1.Location = New System.Drawing.Point(135, 74)
        Me.TextBoxTrackNameT1.Name = "TextBoxTrackNameT1"
        Me.TextBoxTrackNameT1.Size = New System.Drawing.Size(133, 20)
        Me.TextBoxTrackNameT1.TabIndex = 8
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(349, 154)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(34, 13)
        Me.Label54.TabIndex = 7
        Me.Label54.Text = "Price:"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(319, 116)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(64, 13)
        Me.Label53.TabIndex = 6
        Me.Label53.Text = "Size (bytes):"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(311, 77)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(72, 13)
        Me.Label52.TabIndex = 5
        Me.Label52.Text = "Duration (ms):"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(280, 190)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(103, 13)
        Me.Label51.TabIndex = 4
        Me.Label51.Text = "Composer (optional):"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(90, 190)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(39, 13)
        Me.Label50.TabIndex = 3
        Me.Label50.Text = "Genre:"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(63, 154)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(66, 13)
        Me.Label49.TabIndex = 2
        Me.Label49.Text = "Media Type:"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(59, 116)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(70, 13)
        Me.Label48.TabIndex = 1
        Me.Label48.Text = "Album Name:"
        '
        'LabelTrackNameT1
        '
        Me.LabelTrackNameT1.AutoSize = True
        Me.LabelTrackNameT1.Location = New System.Drawing.Point(91, 77)
        Me.LabelTrackNameT1.Name = "LabelTrackNameT1"
        Me.LabelTrackNameT1.Size = New System.Drawing.Size(38, 13)
        Me.LabelTrackNameT1.TabIndex = 0
        Me.LabelTrackNameT1.Text = "Name:"
        '
        'TabPageEditTrack
        '
        Me.TabPageEditTrack.Controls.Add(Me.PictureBox7)
        Me.TabPageEditTrack.Controls.Add(Me.ButtonClearTracks)
        Me.TabPageEditTrack.Controls.Add(Me.ButtonDeleteTrack)
        Me.TabPageEditTrack.Controls.Add(Me.ButtonEditTrack)
        Me.TabPageEditTrack.Controls.Add(Me.ButtonSearchTrack)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxAlbumNameT2)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxPriceT2)
        Me.TabPageEditTrack.Controls.Add(Me.ComboBoxGenreT2)
        Me.TabPageEditTrack.Controls.Add(Me.ComboBoxMediaTypeT2)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxComposerT2)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxSizeT2)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxDurationT2)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxTrackNameT2)
        Me.TabPageEditTrack.Controls.Add(Me.Label56)
        Me.TabPageEditTrack.Controls.Add(Me.Label57)
        Me.TabPageEditTrack.Controls.Add(Me.Label58)
        Me.TabPageEditTrack.Controls.Add(Me.Label59)
        Me.TabPageEditTrack.Controls.Add(Me.Label60)
        Me.TabPageEditTrack.Controls.Add(Me.Label61)
        Me.TabPageEditTrack.Controls.Add(Me.Label62)
        Me.TabPageEditTrack.Controls.Add(Me.Label63)
        Me.TabPageEditTrack.Controls.Add(Me.TextBoxTrackIdT2)
        Me.TabPageEditTrack.Controls.Add(Me.Label55)
        Me.TabPageEditTrack.Location = New System.Drawing.Point(169, 4)
        Me.TabPageEditTrack.Name = "TabPageEditTrack"
        Me.TabPageEditTrack.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageEditTrack.Size = New System.Drawing.Size(624, 504)
        Me.TabPageEditTrack.TabIndex = 1
        Me.TabPageEditTrack.Text = "Edit/Remove Track"
        Me.TabPageEditTrack.UseVisualStyleBackColor = True
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = Global.Project.My.Resources.Resources.music_shop_64
        Me.PictureBox7.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(78, 63)
        Me.PictureBox7.TabIndex = 37
        Me.PictureBox7.TabStop = False
        '
        'ButtonClearTracks
        '
        Me.ButtonClearTracks.Location = New System.Drawing.Point(158, 353)
        Me.ButtonClearTracks.Name = "ButtonClearTracks"
        Me.ButtonClearTracks.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClearTracks.TabIndex = 36
        Me.ButtonClearTracks.Text = "Clear"
        Me.ButtonClearTracks.UseVisualStyleBackColor = True
        '
        'ButtonDeleteTrack
        '
        Me.ButtonDeleteTrack.Location = New System.Drawing.Point(386, 353)
        Me.ButtonDeleteTrack.Name = "ButtonDeleteTrack"
        Me.ButtonDeleteTrack.Size = New System.Drawing.Size(83, 23)
        Me.ButtonDeleteTrack.TabIndex = 35
        Me.ButtonDeleteTrack.Text = "Delete Track"
        Me.ButtonDeleteTrack.UseVisualStyleBackColor = True
        '
        'ButtonEditTrack
        '
        Me.ButtonEditTrack.Location = New System.Drawing.Point(276, 353)
        Me.ButtonEditTrack.Name = "ButtonEditTrack"
        Me.ButtonEditTrack.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEditTrack.TabIndex = 34
        Me.ButtonEditTrack.Text = "Edit Track"
        Me.ButtonEditTrack.UseVisualStyleBackColor = True
        '
        'ButtonSearchTrack
        '
        Me.ButtonSearchTrack.Location = New System.Drawing.Point(337, 61)
        Me.ButtonSearchTrack.Name = "ButtonSearchTrack"
        Me.ButtonSearchTrack.Size = New System.Drawing.Size(92, 23)
        Me.ButtonSearchTrack.TabIndex = 33
        Me.ButtonSearchTrack.Text = "Search Track"
        Me.ButtonSearchTrack.UseVisualStyleBackColor = True
        '
        'TextBoxAlbumNameT2
        '
        Me.TextBoxAlbumNameT2.Location = New System.Drawing.Point(120, 183)
        Me.TextBoxAlbumNameT2.Name = "TextBoxAlbumNameT2"
        Me.TextBoxAlbumNameT2.ReadOnly = True
        Me.TextBoxAlbumNameT2.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxAlbumNameT2.TabIndex = 32
        '
        'TextBoxPriceT2
        '
        Me.TextBoxPriceT2.Location = New System.Drawing.Point(399, 221)
        Me.TextBoxPriceT2.Name = "TextBoxPriceT2"
        Me.TextBoxPriceT2.ReadOnly = True
        Me.TextBoxPriceT2.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxPriceT2.TabIndex = 31
        '
        'ComboBoxGenreT2
        '
        Me.ComboBoxGenreT2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxGenreT2.FormattingEnabled = True
        Me.ComboBoxGenreT2.Location = New System.Drawing.Point(120, 257)
        Me.ComboBoxGenreT2.Name = "ComboBoxGenreT2"
        Me.ComboBoxGenreT2.Size = New System.Drawing.Size(125, 21)
        Me.ComboBoxGenreT2.TabIndex = 30
        '
        'ComboBoxMediaTypeT2
        '
        Me.ComboBoxMediaTypeT2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMediaTypeT2.FormattingEnabled = True
        Me.ComboBoxMediaTypeT2.Location = New System.Drawing.Point(120, 221)
        Me.ComboBoxMediaTypeT2.Name = "ComboBoxMediaTypeT2"
        Me.ComboBoxMediaTypeT2.Size = New System.Drawing.Size(125, 21)
        Me.ComboBoxMediaTypeT2.TabIndex = 29
        '
        'TextBoxComposerT2
        '
        Me.TextBoxComposerT2.Location = New System.Drawing.Point(399, 257)
        Me.TextBoxComposerT2.Name = "TextBoxComposerT2"
        Me.TextBoxComposerT2.ReadOnly = True
        Me.TextBoxComposerT2.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxComposerT2.TabIndex = 28
        '
        'TextBoxSizeT2
        '
        Me.TextBoxSizeT2.Location = New System.Drawing.Point(399, 183)
        Me.TextBoxSizeT2.Name = "TextBoxSizeT2"
        Me.TextBoxSizeT2.ReadOnly = True
        Me.TextBoxSizeT2.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxSizeT2.TabIndex = 27
        '
        'TextBoxDurationT2
        '
        Me.TextBoxDurationT2.Location = New System.Drawing.Point(399, 144)
        Me.TextBoxDurationT2.Name = "TextBoxDurationT2"
        Me.TextBoxDurationT2.ReadOnly = True
        Me.TextBoxDurationT2.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxDurationT2.TabIndex = 26
        '
        'TextBoxTrackNameT2
        '
        Me.TextBoxTrackNameT2.Location = New System.Drawing.Point(120, 144)
        Me.TextBoxTrackNameT2.Name = "TextBoxTrackNameT2"
        Me.TextBoxTrackNameT2.ReadOnly = True
        Me.TextBoxTrackNameT2.Size = New System.Drawing.Size(125, 20)
        Me.TextBoxTrackNameT2.TabIndex = 25
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(342, 224)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(34, 13)
        Me.Label56.TabIndex = 24
        Me.Label56.Text = "Price:"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(312, 186)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(64, 13)
        Me.Label57.TabIndex = 23
        Me.Label57.Text = "Size (bytes):"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(304, 147)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(72, 13)
        Me.Label58.TabIndex = 22
        Me.Label58.Text = "Duration (ms):"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(273, 260)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(103, 13)
        Me.Label59.TabIndex = 21
        Me.Label59.Text = "Composer (optional):"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(75, 260)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(39, 13)
        Me.Label60.TabIndex = 20
        Me.Label60.Text = "Genre:"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(48, 224)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(66, 13)
        Me.Label61.TabIndex = 19
        Me.Label61.Text = "Media Type:"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(44, 186)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(70, 13)
        Me.Label62.TabIndex = 18
        Me.Label62.Text = "Album Name:"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(76, 147)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(38, 13)
        Me.Label63.TabIndex = 17
        Me.Label63.Text = "Name:"
        '
        'TextBoxTrackIdT2
        '
        Me.TextBoxTrackIdT2.Location = New System.Drawing.Point(185, 63)
        Me.TextBoxTrackIdT2.Name = "TextBoxTrackIdT2"
        Me.TextBoxTrackIdT2.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxTrackIdT2.TabIndex = 1
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(101, 66)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(50, 13)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "Track Id:"
        '
        'TabPageListAllTracks
        '
        Me.TabPageListAllTracks.Controls.Add(Me.DataGridViewListAllTracks)
        Me.TabPageListAllTracks.Location = New System.Drawing.Point(169, 4)
        Me.TabPageListAllTracks.Name = "TabPageListAllTracks"
        Me.TabPageListAllTracks.Size = New System.Drawing.Size(624, 504)
        Me.TabPageListAllTracks.TabIndex = 2
        Me.TabPageListAllTracks.Text = "List All"
        Me.TabPageListAllTracks.UseVisualStyleBackColor = True
        '
        'DataGridViewListAllTracks
        '
        Me.DataGridViewListAllTracks.AllowUserToAddRows = False
        Me.DataGridViewListAllTracks.AllowUserToDeleteRows = False
        Me.DataGridViewListAllTracks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewListAllTracks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewListAllTracks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridViewListAllTracks.Location = New System.Drawing.Point(0, 0)
        Me.DataGridViewListAllTracks.Name = "DataGridViewListAllTracks"
        Me.DataGridViewListAllTracks.ReadOnly = True
        Me.DataGridViewListAllTracks.Size = New System.Drawing.Size(624, 504)
        Me.DataGridViewListAllTracks.TabIndex = 0
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(805, 538)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "GUI"
        Me.Text = "Music Shop"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPageCustomers.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPageAddCustomer.ResumeLayout(False)
        Me.TabPageAddCustomer.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageViewCustomer.ResumeLayout(False)
        Me.TabPageViewCustomer.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageRemoveCustomer.ResumeLayout(False)
        Me.TabPageRemoveCustomer.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageEditCustomer.ResumeLayout(False)
        Me.TabPageEditCustomer.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageListAllCustomers.ResumeLayout(False)
        CType(Me.DataGridViewCustomers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageInvoices.ResumeLayout(False)
        Me.TabPageInvoices.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridViewInvoices, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageSongs.ResumeLayout(False)
        Me.TabControl3.ResumeLayout(False)
        Me.TabPageAddTrack.ResumeLayout(False)
        Me.TabPageAddTrack.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPageEditTrack.ResumeLayout(False)
        Me.TabPageEditTrack.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageListAllTracks.ResumeLayout(False)
        CType(Me.DataGridViewListAllTracks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPageCustomers As TabPage
    Friend WithEvents TabPageInvoices As TabPage
    Friend WithEvents TabPageSongs As TabPage
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPageAddCustomer As TabPage
    Friend WithEvents TabPageViewCustomer As TabPage
    Friend WithEvents TabPageRemoveCustomer As TabPage
    Friend WithEvents TabPageEditCustomer As TabPage
    Friend WithEvents TabPageListAllCustomers As TabPage
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents ConfirmButton As Button
    Friend WithEvents TextBoxCompany As TextBox
    Friend WithEvents TextBoxAddress As TextBox
    Friend WithEvents TextBoxCountry As TextBox
    Friend WithEvents TextBoxPhone As TextBox
    Friend WithEvents TextBoxEmail As TextBox
    Friend WithEvents TextBoxLastName As TextBox
    Friend WithEvents TextBoxFirstName As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents ButtonView As Button
    Friend WithEvents TextBoxCustomerID As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents ButtonDelete As Button
    Friend WithEvents Search As Button
    Friend WithEvents TextBoxIDRemove As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents ButtonViewt4 As Button
    Friend WithEvents TextBoxIDt4 As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents ButtonEdit As Button
    Friend WithEvents DataGridViewCustomers As DataGridView
    Friend WithEvents TextBoxPostalCode As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents TextBoxState As TextBox
    Friend WithEvents TextBoxCity As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents TextBoxFax As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents TextBoxPCT2 As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TextBoxST2 As TextBox
    Friend WithEvents TextBoxCityT2 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents TextBoxFT2 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBoxCompT2 As TextBox
    Friend WithEvents TextBoxAT2 As TextBox
    Friend WithEvents TextBoxCT2 As TextBox
    Friend WithEvents TextBoxPT2 As TextBox
    Friend WithEvents TextBoxET2 As TextBox
    Friend WithEvents TextBoxLNT2 As TextBox
    Friend WithEvents TextBoxFNT2 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents TextBoxPCT3 As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents TextBoxST3 As TextBox
    Friend WithEvents TextBoxCityT3 As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents TextBoxFT3 As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents TextBoxCOT3 As TextBox
    Friend WithEvents TextBoxAT3 As TextBox
    Friend WithEvents TextBoxCT3 As TextBox
    Friend WithEvents TextBoxPT3 As TextBox
    Friend WithEvents TextBoxET3 As TextBox
    Friend WithEvents TextBoxLNT3 As TextBox
    Friend WithEvents TextBoxFNT3 As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents TextBoxPCT4 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents TextBoxST4 As TextBox
    Friend WithEvents TextBoxCityT4 As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents TextBoxFT4 As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents TextBoxCOT4 As TextBox
    Friend WithEvents TextBoxAT4 As TextBox
    Friend WithEvents TextBoxCT4 As TextBox
    Friend WithEvents TextBoxPT4 As TextBox
    Friend WithEvents TextBoxET4 As TextBox
    Friend WithEvents TextBoxLNT4 As TextBox
    Friend WithEvents TextBoxFNT4 As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents Label45 As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents ButtonClearT2 As Button
    Friend WithEvents TabControl3 As TabControl
    Friend WithEvents TabPageAddTrack As TabPage
    Friend WithEvents TabPageEditTrack As TabPage
    Friend WithEvents TabPageListAllTracks As TabPage
    Friend WithEvents TextBoxComposerT1 As TextBox
    Friend WithEvents TextBoxSizeT1 As TextBox
    Friend WithEvents TextBoxDurationT1 As TextBox
    Friend WithEvents TextBoxTrackNameT1 As TextBox
    Friend WithEvents Label54 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Label52 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents LabelTrackNameT1 As Label
    Friend WithEvents ComboBoxMediaTypeT1 As ComboBox
    Friend WithEvents ComboBoxGenreT1 As ComboBox
    Friend WithEvents TextBoxPriceT1 As TextBox
    Friend WithEvents TextBoxAlbumNameT1 As TextBox
    Friend WithEvents ButtonAddTrack As Button
    Friend WithEvents TextBoxAlbumNameT2 As TextBox
    Friend WithEvents TextBoxPriceT2 As TextBox
    Friend WithEvents ComboBoxGenreT2 As ComboBox
    Friend WithEvents ComboBoxMediaTypeT2 As ComboBox
    Friend WithEvents TextBoxComposerT2 As TextBox
    Friend WithEvents TextBoxSizeT2 As TextBox
    Friend WithEvents TextBoxDurationT2 As TextBox
    Friend WithEvents TextBoxTrackNameT2 As TextBox
    Friend WithEvents Label56 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents TextBoxTrackIdT2 As TextBox
    Friend WithEvents Label55 As Label
    Friend WithEvents ButtonSearchTrack As Button
    Friend WithEvents ButtonDeleteTrack As Button
    Friend WithEvents ButtonEditTrack As Button
    Friend WithEvents DataGridViewListAllTracks As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents ButtonAddAlbum As Button
    Friend WithEvents Label65 As Label
    Friend WithEvents TextBoxArtistName As TextBox
    Friend WithEvents Label64 As Label
    Friend WithEvents TextBoxAlbumName As TextBox
    Friend WithEvents Label66 As Label
    Friend WithEvents ButtonAddToInvoices As Button
    Friend WithEvents TextBoxQuantityInvoices As TextBox
    Friend WithEvents Label68 As Label
    Friend WithEvents TextBoxTrackIdInvoices As TextBox
    Friend WithEvents Label67 As Label
    Friend WithEvents TextBoxCustomerIdInvoices As TextBox
    Friend WithEvents ButtonCreateInvoice As Button
    Friend WithEvents ButtonClearInvoices As Button
    Friend WithEvents DataGridViewInvoices As DataGridView
    Friend WithEvents ButtonClearTracks As Button
    Friend WithEvents ButtonClearCustomersT1 As Button
    Friend WithEvents ButtonPrint As Button
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents PictureBox7 As PictureBox
End Class
