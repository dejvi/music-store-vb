﻿Imports System.Data.SQLite

Public Class MediaType
    Private _name As String

    Public Property Name As Integer
        Get
            Return _name
        End Get
        Set(value As Integer)
            _name = value
        End Set
    End Property

    Public Shared Function getMediaTypes(ByVal ConnectionString As String) As DataTable
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim ListMediaTypes As String = "SELECT * FROM media_types"
            Dim cmd As New SQLiteCommand(ListMediaTypes, Sqlconn)
            Dim da As New SQLiteDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            da.Fill(dt)
            Sqlconn.Close()
            Return dt
        End Using
    End Function

    Public Shared Function GetMediaType(ByVal ConnectionString As String, ByVal id As Integer) As String
        Dim type As String
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getMediaTypeName As String = "SELECT Name FROM media_types WHERE MediaTypeId = @MediaTypeId"
            Dim cmd As New SQLiteCommand(getMediaTypeName, Sqlconn)
            cmd.Parameters.AddWithValue("@MediaTypeId", id)
            Dim Reader As SQLiteDataReader = cmd.ExecuteReader()
            Using Reader
                While (Reader.Read())
                    type = Reader.GetString(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return type
    End Function

    Public Shared Function GetMediaTypeId(ByVal ConnectionString As String, ByVal name As String) As Integer
        Dim id As Integer
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getId As String = "SELECT MediaTypeId FROM media_types WHERE Name = @Name"
            Dim MediaTypecmd As New SQLiteCommand(getId, Sqlconn)
            MediaTypecmd.Parameters.AddWithValue("@Name", name)
            Dim MediaTypeReader As SQLiteDataReader = MediaTypecmd.ExecuteReader()
            Using MediaTypeReader
                While (MediaTypeReader.Read())
                    id = MediaTypeReader.GetInt32(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return id
    End Function

End Class
