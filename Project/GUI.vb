﻿Imports System.Data.SQLite
Imports System.Text.RegularExpressions

Public Class GUI

    Dim mydb As New DBManager()

    Dim invoicesTable As New DataTable
    Private bitmap As Bitmap

    Private Sub TabControl2_DrawItem(sender As Object, e As DrawItemEventArgs) Handles TabControl2.DrawItem
        Dim g As Graphics = e.Graphics
        Dim _TextBrush As Brush

        ' Get the item from the collection.
        Dim _TabPage As TabPage = TabControl2.TabPages(e.Index)

        ' Get the real bounds for the tab rectangle.
        Dim _TabBounds As Rectangle = TabControl2.GetTabRect(e.Index)

        If (e.State = DrawItemState.Selected) Then
            ' Draw a different background color, and don't paint a focus rectangle.
            _TextBrush = New SolidBrush(Color.Red)
            g.FillRectangle(Brushes.Gray, e.Bounds)
        Else
            _TextBrush = New System.Drawing.SolidBrush(e.ForeColor)
            e.DrawBackground()
        End If

        ' Use our own font.
        Dim _TabFont As New Font("Arial", 10.0, FontStyle.Bold, GraphicsUnit.Pixel)

        ' Draw string. Center the text.
        Dim _StringFlags As New StringFormat()
        _StringFlags.Alignment = StringAlignment.Center
        _StringFlags.LineAlignment = StringAlignment.Center
        g.DrawString(_TabPage.Text, _TabFont, _TextBrush, _TabBounds, New StringFormat(_StringFlags))
    End Sub

    Private Sub ConfirmButton_Click(sender As Object, e As EventArgs) Handles ConfirmButton.Click
        If TextBoxFirstName.Text.Equals(String.Empty) Or TextBoxLastName.Text.Equals(String.Empty) Or TextBoxPhone.Text.Equals(String.Empty) Or
            TextBoxEmail.Text.Equals(String.Empty) Or TextBoxAddress.Text.Equals(String.Empty) Or TextBoxCountry.Text.Equals(String.Empty) Then
            MessageBox.Show("Please fill our all the required fields.")
        Else
            Dim newCustomer As New Customer(TextBoxFirstName.Text, TextBoxLastName.Text, TextBoxAddress.Text, TextBoxCountry.Text, TextBoxEmail.Text,
                                            TextBoxPhone.Text, TextBoxCompany.Text, TextBoxCity.Text, TextBoxState.Text, TextBoxPostalCode.Text,
                                            TextBoxFax.Text)
            newCustomer.addCustomer(mydb.ConnectionString)
            MessageBox.Show("Customer added successfully.")
            TextBoxFirstName.Text = ""
            TextBoxLastName.Text = ""
            TextBoxAddress.Text = ""
            TextBoxCountry.Text = ""
            TextBoxEmail.Text = ""
            TextBoxPhone.Text = ""
            TextBoxCompany.Text = ""
            TextBoxCity.Text = ""
            TextBoxState.Text = ""
            TextBoxPostalCode.Text = ""
            TextBoxFax.Text = ""
            DataGridViewCustomers.DataSource = Customer.ListAllCustomers(mydb.ConnectionString)
        End If


    End Sub

    Private Sub ButtonDelete_Click(sender As Object, e As EventArgs) Handles ButtonDelete.Click
        Customer.removeCustomer(mydb.ConnectionString, Convert.ToInt32(TextBoxIDRemove.Text))
        MessageBox.Show(String.Format("Customer with ID {0} removed successfully.", TextBoxIDRemove.Text))
        TextBoxFNT3.Text = ""
        TextBoxLNT3.Text = ""
        TextBoxCOT3.Text = ""
        TextBoxAT3.Text = ""
        TextBoxCityT3.Text = ""
        TextBoxST3.Text = ""
        TextBoxCT3.Text = ""
        TextBoxPCT3.Text = ""
        TextBoxPT3.Text = ""
        TextBoxFT3.Text = ""
        TextBoxET3.Text = ""
        TextBoxIDRemove.Text = ""
        DataGridViewCustomers.DataSource = Customer.ListAllCustomers(mydb.ConnectionString)
    End Sub



    Private Sub ButtonView_Click(sender As Object, e As EventArgs) Handles ButtonView.Click
        Dim CstmrValues() = Customer.viewCustomer(mydb.ConnectionString, Convert.ToInt32(TextBoxCustomerID.Text))
        TextBoxFNT2.Text = CstmrValues(1)
        TextBoxLNT2.Text = CstmrValues(2)
        TextBoxCompT2.Text = CstmrValues(3)
        TextBoxAT2.Text = CstmrValues(4)
        TextBoxCityT2.Text = CstmrValues(5)
        TextBoxST2.Text = CstmrValues(6)
        TextBoxCT2.Text = CstmrValues(7)
        TextBoxPCT2.Text = CstmrValues(8)
        TextBoxPT2.Text = CstmrValues(9)
        TextBoxFT2.Text = CstmrValues(10)
        TextBoxET2.Text = CstmrValues(11)

    End Sub

    Private Sub Search_Click(sender As Object, e As EventArgs) Handles Search.Click
        Dim CstmrValues() = Customer.viewCustomer(mydb.ConnectionString, Convert.ToInt32(TextBoxIDRemove.Text))
        TextBoxFNT3.Text = CstmrValues(1)
        TextBoxLNT3.Text = CstmrValues(2)
        TextBoxCOT3.Text = CstmrValues(3)
        TextBoxAT3.Text = CstmrValues(4)
        TextBoxCityT3.Text = CstmrValues(5)
        TextBoxST3.Text = CstmrValues(6)
        TextBoxCT3.Text = CstmrValues(7)
        TextBoxPCT3.Text = CstmrValues(8)
        TextBoxPT3.Text = CstmrValues(9)
        TextBoxFT3.Text = CstmrValues(10)
        TextBoxET3.Text = CstmrValues(11)
    End Sub

    Private Sub ButtonViewt4_Click(sender As Object, e As EventArgs) Handles ButtonViewt4.Click
        Dim CstmrValues() = Customer.viewCustomer(mydb.ConnectionString, Convert.ToInt32(TextBoxIDt4.Text))
        TextBoxFNT4.Text = CstmrValues(1)
        TextBoxLNT4.Text = CstmrValues(2)
        TextBoxCOT4.Text = CstmrValues(3)
        TextBoxAT4.Text = CstmrValues(4)
        TextBoxCityT4.Text = CstmrValues(5)
        TextBoxST4.Text = CstmrValues(6)
        TextBoxCT4.Text = CstmrValues(7)
        TextBoxPCT4.Text = CstmrValues(8)
        TextBoxPT4.Text = CstmrValues(9)
        TextBoxFT4.Text = CstmrValues(10)
        TextBoxET4.Text = CstmrValues(11)

        TextBoxFNT4.ReadOnly = False
        TextBoxLNT4.ReadOnly = False
        TextBoxCOT4.ReadOnly = False
        TextBoxAT4.ReadOnly = False
        TextBoxCityT4.ReadOnly = False
        TextBoxST4.ReadOnly = False
        TextBoxCT4.ReadOnly = False
        TextBoxPCT4.ReadOnly = False
        TextBoxPT4.ReadOnly = False
        TextBoxFT4.ReadOnly = False
        TextBoxET4.ReadOnly = False

    End Sub

    Private Sub ButtonEdit_Click(sender As Object, e As EventArgs) Handles ButtonEdit.Click
        If TextBoxFNT4.Text.Equals(String.Empty) Or TextBoxLNT4.Text.Equals(String.Empty) Or TextBoxPT4.Text.Equals(String.Empty) Or
            TextBoxET4.Text.Equals(String.Empty) Or TextBoxAT4.Text.Equals(String.Empty) Or TextBoxCT4.Text.Equals(String.Empty) Then
            MessageBox.Show("Please fill our all the required fields.")
        Else
            Dim newCustomer As New Customer(TextBoxFNT4.Text, TextBoxLNT4.Text, TextBoxAT4.Text, TextBoxCT4.Text, TextBoxET4.Text,
                                            TextBoxPT4.Text, TextBoxCOT4.Text, TextBoxCityT4.Text, TextBoxST4.Text, TextBoxPCT4.Text,
                                            TextBoxFT4.Text)
            newCustomer.editCustomer(mydb.ConnectionString, Convert.ToInt32(TextBoxIDt4.Text))
            MessageBox.Show("Customer edited successfully.")

            TextBoxFNT4.Text = ""
            TextBoxLNT4.Text = ""
            TextBoxCOT4.Text = ""
            TextBoxAT4.Text = ""
            TextBoxCityT4.Text = ""
            TextBoxST4.Text = ""
            TextBoxCT4.Text = ""
            TextBoxPCT4.Text = ""
            TextBoxPT4.Text = ""
            TextBoxFT4.Text = ""
            TextBoxET4.Text = ""
            TextBoxIDt4.Text = ""

            TextBoxFNT4.ReadOnly = True
            TextBoxLNT4.ReadOnly = True
            TextBoxCOT4.ReadOnly = True
            TextBoxAT4.ReadOnly = True
            TextBoxCityT4.ReadOnly = True
            TextBoxST4.ReadOnly = True
            TextBoxCT4.ReadOnly = True
            TextBoxPCT4.ReadOnly = True
            TextBoxPT4.ReadOnly = True
            TextBoxFT4.ReadOnly = True
            TextBoxET4.ReadOnly = True

            DataGridViewCustomers.DataSource = Customer.ListAllCustomers(mydb.ConnectionString)

        End If
    End Sub

    Private Sub ButtonListAllCustomers_Click(sender As Object, e As EventArgs)
        DataGridViewCustomers.DataSource = Customer.ListAllCustomers(mydb.ConnectionString)
    End Sub

    Private Sub ButtonClearT2_Click(sender As Object, e As EventArgs) Handles ButtonClearT2.Click
        TextBoxFNT2.Text = ""
        TextBoxLNT2.Text = ""
        TextBoxCompT2.Text = ""
        TextBoxAT2.Text = ""
        TextBoxCityT2.Text = ""
        TextBoxST2.Text = ""
        TextBoxCT2.Text = ""
        TextBoxPCT2.Text = ""
        TextBoxPT2.Text = ""
        TextBoxFT2.Text = ""
        TextBoxET2.Text = ""
        TextBoxCustomerID.Text = ""
    End Sub

    Private Sub TabControl3_DrawItem(sender As Object, e As DrawItemEventArgs) Handles TabControl3.DrawItem
        Dim g As Graphics = e.Graphics
        Dim _TextBrush As Brush

        ' Get the item from the collection.
        Dim _TabPage As TabPage = TabControl3.TabPages(e.Index)

        ' Get the real bounds for the tab rectangle.
        Dim _TabBounds As Rectangle = TabControl3.GetTabRect(e.Index)

        If (e.State = DrawItemState.Selected) Then
            ' Draw a different background color, and don't paint a focus rectangle.
            _TextBrush = New SolidBrush(Color.Red)
            g.FillRectangle(Brushes.Gray, e.Bounds)
        Else
            _TextBrush = New System.Drawing.SolidBrush(e.ForeColor)
            e.DrawBackground()
        End If

        ' Use our own font.
        Dim _TabFont As New Font("Arial", 10.0, FontStyle.Bold, GraphicsUnit.Pixel)

        ' Draw string. Center the text.
        Dim _StringFlags As New StringFormat()
        _StringFlags.Alignment = StringAlignment.Center
        _StringFlags.LineAlignment = StringAlignment.Center
        g.DrawString(_TabPage.Text, _TabFont, _TextBrush, _TabBounds, New StringFormat(_StringFlags))
    End Sub

    Private Sub GUI_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim dtMediaType, dtGenre As New DataTable

        DataGridViewCustomers.DataSource = Customer.ListAllCustomers(mydb.ConnectionString)
        DataGridViewListAllTracks.DataSource = Track.ListAllTracks(mydb.ConnectionString)

        dtMediaType = MediaType.getMediaTypes(mydb.ConnectionString)
        ComboBoxMediaTypeT1.DataSource = dtMediaType
        ComboBoxMediaTypeT1.DisplayMember = "Name"
        ComboBoxMediaTypeT1.ValueMember = "MediaTypeID"

        dtGenre = Genre.getGenres(mydb.ConnectionString)
        ComboBoxGenreT1.DataSource = dtGenre
        ComboBoxGenreT1.DisplayMember = "Name"
        ComboBoxGenreT1.ValueMember = "GenreId"

        TextBoxAlbumNameT1.AutoCompleteSource = AutoCompleteSource.CustomSource
        TextBoxAlbumNameT1.AutoCompleteCustomSource = Album.GetAlbumNames(mydb.ConnectionString)
        TextBoxAlbumNameT1.AutoCompleteMode = AutoCompleteMode.Suggest

        dtMediaType = MediaType.getMediaTypes(mydb.ConnectionString)
        ComboBoxMediaTypeT2.DataSource = dtMediaType
        ComboBoxMediaTypeT2.DisplayMember = "Name"
        ComboBoxMediaTypeT2.ValueMember = "MediaTypeID"

        dtGenre = Genre.getGenres(mydb.ConnectionString)
        ComboBoxGenreT2.DataSource = dtGenre
        ComboBoxGenreT2.DisplayMember = "Name"
        ComboBoxGenreT2.ValueMember = "GenreId"

        TextBoxAlbumNameT2.AutoCompleteSource = AutoCompleteSource.CustomSource
        TextBoxAlbumNameT2.AutoCompleteCustomSource = Album.GetAlbumNames(mydb.ConnectionString)
        TextBoxAlbumNameT2.AutoCompleteMode = AutoCompleteMode.Suggest

        TextBoxArtistName.AutoCompleteSource = AutoCompleteSource.CustomSource
        TextBoxArtistName.AutoCompleteCustomSource = Artist.GetArtistNames(mydb.ConnectionString)
        TextBoxArtistName.AutoCompleteMode = AutoCompleteMode.Suggest


        invoicesTable.Columns.Add("Name", GetType(String))
        invoicesTable.Columns.Add("Album", GetType(String))
        invoicesTable.Columns.Add("Media Type", GetType(String))
        invoicesTable.Columns.Add("Genre", GetType(String))
        invoicesTable.Columns.Add("Duration", GetType(Integer))
        invoicesTable.Columns.Add("Size", GetType(Integer))
        invoicesTable.Columns.Add("Composer", GetType(String))
        invoicesTable.Columns.Add("Price", GetType(Decimal))
        invoicesTable.Columns.Add("Quantity", GetType(Integer))
        DataGridViewInvoices.DataSource = invoicesTable
    End Sub

    Private Sub ButtonAddTrack_Click(sender As Object, e As EventArgs) Handles ButtonAddTrack.Click
        If TextBoxAlbumNameT1.Text.Equals(String.Empty) Or TextBoxTrackNameT1.Text.Equals(String.Empty) Or TextBoxDurationT1.Text.Equals(String.Empty) Or
            TextBoxSizeT1.Text.Equals(String.Empty) Or TextBoxPriceT1.Text.Equals(String.Empty) Then
            MessageBox.Show("Please fill our all the required fields.")
        Else
            Dim newTrack As New Track(TextBoxTrackNameT1.Text, Convert.ToInt32(TextBoxSizeT1.Text), Convert.ToInt32(TextBoxDurationT1.Text),
                                      Convert.ToDecimal(TextBoxPriceT1.Text), TextBoxComposerT1.Text)
            newTrack.addTrack(mydb.ConnectionString, Album.GetAlbumId(mydb.ConnectionString, TextBoxAlbumNameT1.Text),
                              MediaType.GetMediaTypeId(mydb.ConnectionString, ComboBoxMediaTypeT1.Text), Genre.GetGenreId(mydb.ConnectionString, ComboBoxGenreT1.Text))
            MessageBox.Show("Track added successfully.")
            TextBoxTrackNameT1.Text = ""
            TextBoxAlbumNameT1.Text = ""
            TextBoxDurationT1.Text = ""
            TextBoxSizeT1.Text = ""
            TextBoxComposerT1.Text = ""
            ComboBoxMediaTypeT1.Text = ""
            ComboBoxGenreT1.Text = ""
            TextBoxPriceT1.Text = ""
            DataGridViewListAllTracks.DataSource = Track.ListAllTracks(mydb.ConnectionString)

        End If

    End Sub

    Private Sub ButtonSearchTrack_Click(sender As Object, e As EventArgs) Handles ButtonSearchTrack.Click
        Dim TrackValues() = Track.viewTrack(mydb.ConnectionString, Convert.ToInt32(TextBoxTrackIdT2.Text))
        TextBoxTrackNameT2.Text = TrackValues(1)
        TextBoxAlbumNameT2.Text = Album.GetAlbumName(mydb.ConnectionString, Convert.ToInt32(TrackValues(2)))
        TextBoxDurationT2.Text = TrackValues(6)
        TextBoxSizeT2.Text = TrackValues(7)
        TextBoxComposerT2.Text = TrackValues(5)
        ComboBoxMediaTypeT2.Text = MediaType.GetMediaType(mydb.ConnectionString, Convert.ToInt32(TrackValues(3)))
        ComboBoxGenreT2.Text = Genre.GetGenre(mydb.ConnectionString, Convert.ToInt32(TrackValues(4)))
        TextBoxPriceT2.Text = TrackValues(8)

        TextBoxTrackNameT2.ReadOnly = False
        TextBoxAlbumNameT2.ReadOnly = False
        TextBoxDurationT2.ReadOnly = False
        TextBoxSizeT2.ReadOnly = False
        TextBoxComposerT2.ReadOnly = False
        TextBoxPriceT2.ReadOnly = False
    End Sub

    Private Sub ButtonEditTrack_Click(sender As Object, e As EventArgs) Handles ButtonEditTrack.Click
        If TextBoxAlbumNameT2.Text.Equals(String.Empty) Or TextBoxTrackNameT2.Text.Equals(String.Empty) Or TextBoxDurationT2.Text.Equals(String.Empty) Or
            TextBoxSizeT2.Text.Equals(String.Empty) Or TextBoxPriceT2.Text.Equals(String.Empty) Then
            MessageBox.Show("Please fill our all the required fields.")
        Else
            Dim newTrack As New Track(TextBoxTrackNameT2.Text, Convert.ToInt32(TextBoxSizeT2.Text), Convert.ToInt32(TextBoxDurationT2.Text),
                                      Convert.ToDecimal(TextBoxPriceT2.Text), TextBoxComposerT2.Text)
            newTrack.editTrack(mydb.ConnectionString, Convert.ToInt32(TextBoxTrackIdT2.Text), Album.GetAlbumId(mydb.ConnectionString, TextBoxAlbumNameT2.Text),
                              MediaType.GetMediaTypeId(mydb.ConnectionString, ComboBoxMediaTypeT2.Text), Genre.GetGenreId(mydb.ConnectionString, ComboBoxGenreT2.Text))
            MessageBox.Show("Track edited successfully.")
            TextBoxTrackNameT2.Text = ""
            TextBoxAlbumNameT2.Text = ""
            TextBoxDurationT2.Text = ""
            TextBoxSizeT2.Text = ""
            TextBoxComposerT2.Text = ""
            ComboBoxMediaTypeT2.Text = ""
            ComboBoxGenreT2.Text = ""
            TextBoxPriceT2.Text = ""

            TextBoxTrackNameT2.ReadOnly = True
            TextBoxAlbumNameT2.ReadOnly = True
            TextBoxDurationT2.ReadOnly = True
            TextBoxSizeT2.ReadOnly = True
            TextBoxComposerT2.ReadOnly = True
            TextBoxPriceT2.ReadOnly = True
            ComboBoxGenreT2.Text = "Rock"
            ComboBoxMediaTypeT2.Text = "MPEG audio file"
            DataGridViewListAllTracks.DataSource = Track.ListAllTracks(mydb.ConnectionString)

        End If
    End Sub

    Private Sub ButtonDeleteTrack_Click(sender As Object, e As EventArgs) Handles ButtonDeleteTrack.Click
        Track.removeTrack(mydb.ConnectionString, Convert.ToInt32(TextBoxTrackIdT2.Text))
        MessageBox.Show("Track deleted successfully.")

        TextBoxTrackNameT2.ReadOnly = True
        TextBoxAlbumNameT2.ReadOnly = True
        TextBoxDurationT2.ReadOnly = True
        TextBoxSizeT2.ReadOnly = True
        TextBoxComposerT2.ReadOnly = True
        TextBoxPriceT2.ReadOnly = True
        TextBoxTrackNameT2.Text = ""
        TextBoxAlbumNameT2.Text = ""
        TextBoxDurationT2.Text = ""
        TextBoxSizeT2.Text = ""
        TextBoxComposerT2.Text = ""
        ComboBoxMediaTypeT2.Text = ""
        ComboBoxGenreT2.Text = ""
        TextBoxPriceT2.Text = ""
        DataGridViewListAllTracks.DataSource = Track.ListAllTracks(mydb.ConnectionString)
    End Sub


    Private Sub ButtonAddAlbum_Click(sender As Object, e As EventArgs) Handles ButtonAddAlbum.Click
        Album.addAlbum(mydb.ConnectionString, Artist.GetArtistId(mydb.ConnectionString, TextBoxArtistName.Text), TextBoxAlbumName.Text)
        MessageBox.Show("Album added successfully.")
        TextBoxArtistName.Text = ""
        TextBoxAlbumName.Text = ""
        DataGridViewListAllTracks.DataSource = Track.ListAllTracks(mydb.ConnectionString)
    End Sub



    Private Sub ButtonAddToInvoices_Click(sender As Object, e As EventArgs) Handles ButtonAddToInvoices.Click
        TextBoxCustomerIdInvoices.ReadOnly = True
        Dim TrackValues() = Track.viewTrack(mydb.ConnectionString, Convert.ToInt32(TextBoxTrackIdInvoices.Text))
        invoicesTable.Rows.Add(TrackValues(1), Album.GetAlbumName(mydb.ConnectionString, Convert.ToInt32(TrackValues(2))),
                               MediaType.GetMediaType(mydb.ConnectionString, Convert.ToInt32(TrackValues(3))), Genre.GetGenre(mydb.ConnectionString, Convert.ToInt32(TrackValues(4))),
                               Convert.ToInt32(TrackValues(6)), Convert.ToInt32(TrackValues(7)), TrackValues(5),
                               Convert.ToDecimal(TrackValues(8)), Convert.ToInt32(TextBoxQuantityInvoices.Text))
        DataGridViewInvoices.Update()
    End Sub

    Private Sub ButtonClearInvoices_Click(sender As Object, e As EventArgs) Handles ButtonClearInvoices.Click
        TextBoxCustomerIdInvoices.ReadOnly = False
        TextBoxCustomerIdInvoices.Text = ""
        TextBoxTrackIdInvoices.Text = ""
        TextBoxQuantityInvoices.Text = ""
        invoicesTable.Clear()
        DataGridViewInvoices.Update()
    End Sub

    Private Sub ButtonCreateInvoice_Click(sender As Object, e As EventArgs) Handles ButtonCreateInvoice.Click
        Dim total As Decimal
        total = 0
        If invoicesTable.Rows.Count > 0 Then
            For i = 0 To invoicesTable.Rows.Count - 1
                total = total + (invoicesTable.Rows(i)(7) * invoicesTable.Rows(i)(8))
            Next
        End If

        Dim d As DateTime = DateTime.Now

        Invoice.addTrack(mydb.ConnectionString, Convert.ToInt32(TextBoxCustomerIdInvoices.Text), total, d)

        If invoicesTable.Rows.Count > 0 Then
            For i = 0 To invoicesTable.Rows.Count - 1
                Using Sqlconn As New SQLiteConnection(mydb.ConnectionString)
                    Sqlconn.Open()
                    Dim insertInvoiceItem As String = "INSERT INTO invoice_items(InvoiceId, TrackId, UnitPrice, Quantity) 
                                                       VALUES ((SELECT InvoiceId FROM invoices WHERE InvoiceDate = @date), 
                                                       (SELECT TrackId FROM tracks WHERE Name=@name),
                                                       @UnitPrice, @Quantity) "
                    Dim cmd As New SQLiteCommand(insertInvoiceItem, Sqlconn)
                    cmd.Parameters.AddWithValue("@date", d)
                    cmd.Parameters.AddWithValue("@name", invoicesTable.Rows(i)(0))
                    cmd.Parameters.AddWithValue("@UnitPrice", invoicesTable.Rows(i)(7))
                    cmd.Parameters.AddWithValue("@Quantity", invoicesTable.Rows(i)(8))
                    cmd.ExecuteNonQuery()
                    Sqlconn.Close()
                End Using
            Next
        End If

        ButtonClearInvoices_Click(sender, e)
    End Sub

    Private Sub TextBoxDurationT1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxDurationT1.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxSizeT1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxSizeT1.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxQuantityInvoices_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxQuantityInvoices.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxCustomerIdInvoices_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxCustomerIdInvoices.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxTrackIdInvoices_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxTrackIdInvoices.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxTrackIdT2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxTrackIdT2.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxCustomerID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxCustomerID.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxIDRemove_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxIDRemove.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxIDt4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxIDt4.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ButtonClearTracks_Click(sender As Object, e As EventArgs) Handles ButtonClearTracks.Click
        TextBoxTrackNameT2.Text = ""
        TextBoxAlbumNameT2.Text = ""
        TextBoxDurationT2.Text = ""
        TextBoxSizeT2.Text = ""
        TextBoxComposerT2.Text = ""
        ComboBoxMediaTypeT2.Text = ""
        ComboBoxGenreT2.Text = ""
        TextBoxPriceT2.Text = ""

        TextBoxTrackNameT2.ReadOnly = True
        TextBoxAlbumNameT2.ReadOnly = True
        TextBoxDurationT2.ReadOnly = True
        TextBoxSizeT2.ReadOnly = True
        TextBoxComposerT2.ReadOnly = True
        TextBoxPriceT2.ReadOnly = True
        ComboBoxGenreT2.Text = "Rock"
        ComboBoxMediaTypeT2.Text = "MPEG audio file"
    End Sub

    Private Sub ButtonClearCustomersT1_Click(sender As Object, e As EventArgs) Handles ButtonClearCustomersT1.Click
        TextBoxFirstName.Text = ""
        TextBoxLastName.Text = ""
        TextBoxAddress.Text = ""
        TextBoxCountry.Text = ""
        TextBoxEmail.Text = ""
        TextBoxPhone.Text = ""
        TextBoxCompany.Text = ""
        TextBoxCity.Text = ""
        TextBoxState.Text = ""
        TextBoxPostalCode.Text = ""
        TextBoxFax.Text = ""
    End Sub

    Private Sub ButtonPrint_Click(sender As Object, e As EventArgs) Handles ButtonPrint.Click
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        bitmap = New Bitmap(Me.DataGridViewInvoices.Width, Me.DataGridViewInvoices.Height)
        DataGridViewInvoices.DrawToBitmap(bitmap, New Rectangle(0, 0, Me.DataGridViewInvoices.Width, Me.DataGridViewInvoices.Height))
        e.Graphics.DrawImage(bitmap, 10, 10)

    End Sub

    Private Sub TextBoxFirstName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFirstName.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyz"
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxLastName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxLastName.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyz"
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxLNT4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxLNT4.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyz"
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxFNT4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFNT4.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyz"
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Function IsValidEmailFormat(ByVal s As String) As Boolean
        Return Regex.IsMatch(s, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")
    End Function

    Private Sub TextBoxEmail_TextChanged(sender As Object, e As EventArgs) Handles TextBoxEmail.TextChanged
        If IsValidEmailFormat(TextBoxEmail.Text) Then
            TextBoxEmail.ForeColor = Color.Black
        Else
            TextBoxEmail.ForeColor = Color.Red
        End If
    End Sub
End Class