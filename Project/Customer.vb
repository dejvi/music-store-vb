﻿Imports System.Data.SQLite
Imports System.IO
Public Class Customer
    Private _firstName As String
    Private _lastName As String
    Private _company As String
    Private _email As String
    Private _phoneNumber As String
    Private _country As String
    Private _address As String
    Private _city As String
    Private _state As String
    Private _postalCode As String
    Private _fax As String

    Public Sub New(ByVal FirstName As String, ByVal LastName As String, ByVal Address As String, ByVal Country As String, ByVal Email As String,
                   ByVal Phone As String, Optional ByRef Company As String = Nothing, Optional ByVal City As String = Nothing,
                   Optional ByVal State As String = Nothing, Optional ByVal PostalCode As String = Nothing, Optional ByVal Fax As String = Nothing)

        _firstName = FirstName
        _lastName = LastName
        _company = Company
        _email = Email
        _phoneNumber = Phone
        _country = Country
        _address = Address
        _city = City
        _state = State
        _postalCode = PostalCode
        _fax = Fax

    End Sub

    Public Property FirstName As String
        Get
            Return _firstName
        End Get
        Set(value As String)
            _firstName = value
        End Set
    End Property

    Public Property LastName As String
        Get
            Return _lastName
        End Get
        Set(value As String)
            _lastName = value
        End Set
    End Property

    Public Property Company As String
        Get
            Return _company
        End Get
        Set(value As String)
            _company = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property PhoneNumber As String
        Get
            Return _phoneNumber
        End Get
        Set(value As String)
            _phoneNumber = value
        End Set
    End Property

    Public Property Country As String
        Get
            Return _country
        End Get
        Set(value As String)
            _country = value
        End Set
    End Property

    Public Property Address As String
        Get
            Return _address
        End Get
        Set(value As String)
            _address = value
        End Set
    End Property

    Public Property City As String
        Get
            Return _city
        End Get
        Set(value As String)
            _city = value
        End Set
    End Property

    Public Property State As String
        Get
            Return _state
        End Get
        Set(value As String)
            _state = value
        End Set
    End Property

    Public Property PostalCode As String
        Get
            Return _postalCode
        End Get
        Set(value As String)
            _postalCode = value
        End Set
    End Property

    Public Property Fax As String
        Get
            Return _fax
        End Get
        Set(value As String)
            _fax = value
        End Set
    End Property

    Public Sub addCustomer(ByVal ConnectionString As String)
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Dim insertCustomer As String = "INSERT INTO customers(FirstName, LastName, Company, Address, Country, Phone, Email, Fax, City, State, PostalCode, SupportRepId) 
                                            VALUES (@FirstName, @LastName, @Company, @Address, @Country, @Phone, @Email, @Fax, @City, @State, @PostalCode, @SupportRepId)"
            Dim cmd As New SQLiteCommand(insertCustomer, Sqlconn)
            cmd.Parameters.AddWithValue("@FirstName", _firstName)
            cmd.Parameters.AddWithValue("@LastName", _lastName)
            cmd.Parameters.AddWithValue("@Company", _company)
            cmd.Parameters.AddWithValue("@Address", _address)
            cmd.Parameters.AddWithValue("@Country", _country)
            cmd.Parameters.AddWithValue("@Phone", _phoneNumber)
            cmd.Parameters.AddWithValue("@Email", _email)
            cmd.Parameters.AddWithValue("@Fax", _fax)
            cmd.Parameters.AddWithValue("@City", _city)
            cmd.Parameters.AddWithValue("@State", _state)
            cmd.Parameters.AddWithValue("@PostalCode", _postalCode)
            cmd.Parameters.AddWithValue("@SupportRepId", Employee.currentEmplyoeeId)
            Sqlconn.Open()
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

    Public Sub editCustomer(ByVal ConnectionString As String, ByVal ID As Integer)
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Dim insertCustomer As String = "UPDATE customers SET FirstName=@FirstName, LastName=@LastName, Company=@Company, 
                                            Address=@Address, Country=@Country, Phone=@Phone, Email=@Email, Fax=@Fax, City=@City, State=@State, PostalCode=@PostalCode
                                            WHERE CustomerId = @CustomerID"
            Dim cmd As New SQLiteCommand(insertCustomer, Sqlconn)
            cmd.Parameters.AddWithValue("@FirstName", _firstName)
            cmd.Parameters.AddWithValue("@LastName", _lastName)
            cmd.Parameters.AddWithValue("@Company", _company)
            cmd.Parameters.AddWithValue("@Address", _address)
            cmd.Parameters.AddWithValue("@Country", _country)
            cmd.Parameters.AddWithValue("@Phone", _phoneNumber)
            cmd.Parameters.AddWithValue("@Email", _email)
            cmd.Parameters.AddWithValue("@Fax", _fax)
            cmd.Parameters.AddWithValue("@City", _city)
            cmd.Parameters.AddWithValue("@State", _state)
            cmd.Parameters.AddWithValue("@PostalCode", _postalCode)
            cmd.Parameters.AddWithValue("@CustomerId", ID)
            Sqlconn.Open()
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

    Public Shared Sub removeCustomer(ByVal ConnectionString As String, ByVal ID As Integer)
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Dim deleteCustomer As String = "DELETE FROM customers WHERE CustomerId = @CustomerId"
            Dim cmd As New SQLiteCommand(deleteCustomer, Sqlconn)
            cmd.Parameters.AddWithValue("@CustomerId", ID)
            Sqlconn.Open()
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

    Public Shared Function viewCustomer(ByVal ConnectionString As String, ByVal ID As Integer) As String()
        Dim values(12) As String

        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Dim viewCstmr As String = "SELECT * FROM customers WHERE CustomerId = @CustomerId"
            Dim cmd As New SQLiteCommand(viewCstmr, Sqlconn)
            cmd.Parameters.AddWithValue("@CustomerId", ID)
            Sqlconn.Open()
            Dim reader As SQLiteDataReader = cmd.ExecuteReader()
            While reader.Read()
                For i = 0 To reader.FieldCount - 1
                    values(i) = reader.GetValue(i).ToString()
                Next
            End While
            Sqlconn.Close()
        End Using

        Return values
    End Function

    Public Shared Function ListAllCustomers(ByVal ConnectionString As String) As DataTable
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim ListAll As String = "SELECT * FROM customers"
            Dim cmd As New SQLiteCommand(ListAll, Sqlconn)
            Dim da As New SQLiteDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            da.Fill(dt)
            Sqlconn.Close()
            Return dt
        End Using
    End Function

End Class
