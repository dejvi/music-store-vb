﻿Imports System.Data.SQLite

Public Class Album
    ''' <summary>
    ''' Name of the album
    ''' </summary>
    Private _title As String

    Public Property Title As String
        Get
            Return _title
        End Get
        Set(value As String)
            _title = value
        End Set
    End Property

    Public Property Artist As Artist
        Get
            Return Nothing
        End Get
        Set(value As Artist)
        End Set
    End Property

    Public Shared Function GetAlbumNames(ByVal ConnectionString As String) As AutoCompleteStringCollection
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim ListAlbums As String = "SELECT * FROM albums"
            Dim cmd As New SQLiteCommand(ListAlbums, Sqlconn)
            Dim da As New SQLiteDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            Dim col As New AutoCompleteStringCollection
            da.Fill(dt)
            For i = 0 To dt.Rows.Count - 1
                col.Add(dt.Rows(i)("Title").ToString())
            Next
            Sqlconn.Close()
            Return col
        End Using
    End Function

    Public Shared Function GetAlbumName(ByVal ConnectionString As String, ByVal id As Integer) As String
        Dim name As String
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getName As String = "SELECT Title FROM albums WHERE AlbumId = @AlbumId"
            Dim Albumcmd As New SQLiteCommand(getName, Sqlconn)
            Albumcmd.Parameters.AddWithValue("@AlbumId", id)
            Dim AlbumReader As SQLiteDataReader = Albumcmd.ExecuteReader()
            Using AlbumReader
                While (AlbumReader.Read())
                    name = AlbumReader.GetString(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return name
    End Function

    Public Shared Function GetAlbumId(ByVal ConnectionString As String, ByVal name As String) As Integer
        Dim id As Integer
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim getID As String = "SELECT AlbumId FROM albums WHERE Title = @Title"
            Dim Albumcmd As New SQLiteCommand(getID, Sqlconn)
            Albumcmd.Parameters.AddWithValue("@Title", name)
            Dim AlbumReader As SQLiteDataReader = Albumcmd.ExecuteReader()
            Using AlbumReader
                While (AlbumReader.Read())
                    id = AlbumReader.GetInt32(0)
                End While
            End Using
            Sqlconn.Close()
        End Using
        Return id
    End Function

    Public Shared Sub addAlbum(ByVal ConnectionString As String, ByVal id As Integer, ByVal t As String)
        Using sqlconn As New SQLiteConnection(ConnectionString)
            sqlconn.Open()
            Dim insertAlbum As String = "INSERT INTO albums(Title, ArtistId) 
                                            VALUES (@Title, @ArtistId)"
            Dim cmd As New SQLiteCommand(insertAlbum, sqlconn)
            cmd.Parameters.AddWithValue("@Title", t)
            cmd.Parameters.AddWithValue("@ArtistId", id)
            cmd.ExecuteNonQuery()
            sqlconn.Close()
        End Using
    End Sub

End Class
