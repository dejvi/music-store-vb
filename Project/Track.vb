﻿Imports System.Data.SQLite

Public Class Track
    ''' <summary>
    ''' Name of the song
    ''' </summary>
    Private _name As String
    ''' <summary>
    ''' Who composed the song
    ''' </summary>
    Private _composer As String
    ''' <summary>
    ''' Size in Bytes
    ''' </summary>
    Private _size As Integer
    ''' <summary>
    ''' Length of the song in ms
    ''' </summary>
    Private _length As Integer
    ''' <summary>
    ''' Price of a unit
    ''' </summary>
    Private _price As Double

    Public Sub New(ByVal Name As String, ByVal Size As Integer, ByVal Length As Integer, ByVal Price As Decimal,
                   Optional ByRef Composer As String = Nothing)

        _name = Name
        _composer = Composer
        _size = Size
        _length = Length
        _price = Price

    End Sub

    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    Public Property Composer As String
        Get
            Return _composer
        End Get
        Set(value As String)
            _composer = value
        End Set
    End Property

    Public Property Size As Integer
        Get
            Return _size
        End Get
        Set(value As Integer)
            _size = value
        End Set
    End Property

    Public Property Length As Integer
        Get
            Return _length
        End Get
        Set(value As Integer)
            _length = value
        End Set
    End Property

    Public Property Price As Double
        Get
            Return _price
        End Get
        Set(value As Double)
            _price = value
        End Set
    End Property

    Public Property Album As Album
        Get
            Return Nothing
        End Get
        Set(value As Album)
        End Set
    End Property

    Public Property Genre As Genre
        Get
            Return Nothing
        End Get
        Set(value As Genre)
        End Set
    End Property

    Public Property MediaType As MediaType
        Get
            Return Nothing
        End Get
        Set(value As MediaType)
        End Set
    End Property

    Public Property Playlist As Playlist
        Get
            Return Nothing
        End Get
        Set(value As Playlist)
        End Set
    End Property

    Public Sub addTrack(ByVal ConnectionString As String, ByVal AlbumId As Integer, ByVal MediaId As Integer, ByVal GenreId As Integer)
        Using Sqlconn As New SQLiteConnection(ConnectionString)

            Sqlconn.Open()
            Dim insertTrack As String = "INSERT INTO tracks(Name, AlbumId, MediaTypeId, GenreId, Composer, Milliseconds, Bytes, UnitPrice) 
                                            VALUES (@Name, @AlbumId, @MediaTypeId, @GenreId, @Composer, @Milliseconds, @Bytes, @UnitPrice)"
            Dim cmd As New SQLiteCommand(insertTrack, Sqlconn)
            cmd.Parameters.AddWithValue("@Name", _name)
            cmd.Parameters.AddWithValue("@Composer", _composer)
            cmd.Parameters.AddWithValue("@Bytes", _size)
            cmd.Parameters.AddWithValue("@Milliseconds", _length)
            cmd.Parameters.AddWithValue("@UnitPrice", _price)
            cmd.Parameters.AddWithValue("@AlbumId", AlbumId)
            cmd.Parameters.AddWithValue("@MediaTypeId", MediaId)
            cmd.Parameters.AddWithValue("@GenreId", GenreId)
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

    Public Shared Function viewTrack(ByVal ConnectionString As String, ByVal ID As Integer) As String()
        Dim values(9) As String

        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Dim viewTrck As String = "SELECT * FROM tracks WHERE TrackId= @TrackId"
            Dim cmd As New SQLiteCommand(viewTrck, Sqlconn)
            cmd.Parameters.AddWithValue("@TrackId", ID)
            Sqlconn.Open()
            Dim reader As SQLiteDataReader = cmd.ExecuteReader()
            While reader.Read()
                For i = 0 To reader.FieldCount - 1
                    values(i) = reader.GetValue(i).ToString()
                Next
            End While
            Sqlconn.Close()
        End Using

        Return values
    End Function

    Public Sub editTrack(ByVal ConnectionString As String, ByVal ID As Integer, ByVal AlbumID As Integer, ByVal MediaId As Integer, ByVal GenreId As Integer)
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim editTrck As String = "UPDATE tracks SET Name=@Name, AlbumId=@AlbumId, MediaTypeId=@MediaTypeId, GenreId=@GenreId, 
                                            Composer=@Composer, Milliseconds=@Milliseconds, Bytes=@Bytes, UnitPrice=@UnitPrice
                                            WHERE TrackId = @TrackId"
            Dim cmd As New SQLiteCommand(editTrck, Sqlconn)
            cmd.Parameters.AddWithValue("@Name", _name)
            cmd.Parameters.AddWithValue("@Composer", _composer)
            cmd.Parameters.AddWithValue("@Bytes", _size)
            cmd.Parameters.AddWithValue("@Milliseconds", _length)
            cmd.Parameters.AddWithValue("@UnitPrice", _price)
            cmd.Parameters.AddWithValue("@AlbumId", AlbumID)
            cmd.Parameters.AddWithValue("@MediaTypeId", MediaId)
            cmd.Parameters.AddWithValue("@GenreId", GenreId)
            cmd.Parameters.AddWithValue("@TrackId", ID)
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

    Public Shared Sub removeTrack(ByVal ConnectionString As String, ByVal ID As Integer)
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Dim deleteTrack As String = "DELETE FROM tracks WHERE TrackId = @TrackId"
            Dim cmd As New SQLiteCommand(deleteTrack, Sqlconn)
            cmd.Parameters.AddWithValue("@TrackId", ID)
            Sqlconn.Open()
            cmd.ExecuteNonQuery()
            Sqlconn.Close()

        End Using
    End Sub

    Public Shared Function ListAllTracks(ByVal ConnectionString As String) As DataTable
        Using Sqlconn As New SQLiteConnection(ConnectionString)
            Sqlconn.Open()
            Dim ListAll As String = "SELECT tracks.TrackId, tracks.Name, albums.title, media_types.Name, genres.Name, tracks.Composer, tracks.Milliseconds, tracks.Bytes, 
                                     tracks.UnitPrice FROM tracks, albums, media_types, genres
                                     WHERE tracks.AlbumId=albums.AlbumId AND tracks.MediaTypeId=media_types.MediaTypeId AND tracks.GenreId=genres.GenreId
                                     ORDER BY TrackId ASC"
            Dim cmd As New SQLiteCommand(ListAll, Sqlconn)
            Dim da As New SQLiteDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            da.Fill(dt)
            Sqlconn.Close()
            Return dt
        End Using
    End Function

End Class
